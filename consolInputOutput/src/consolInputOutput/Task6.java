package consolInputOutput;

import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		
		int first, second, third, fourth, fifth;
		
		System.out.println("Give 5 numbers and the program will compute the sum of them:");
		
		Scanner scanner = new Scanner(System.in);
		
		first = scanner.nextInt();
		second = scanner.nextInt();
		third = scanner.nextInt();
		fourth = scanner.nextInt();
		fifth = scanner.nextInt();

		int sum = first + second + third + fourth + fifth;

		System.out.println("The sum of 5 numbers is " + sum);
	}

}
