package consolInputOutput;

import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		double radiusCircle;
		
		System.out.println("Enter the radius of the circle here and the program will compute its area!");
		
		Scanner scanner = new Scanner(System.in);
		radiusCircle = scanner.nextDouble();
		
		double area = 2 * Math.PI * radiusCircle;
		
		System.out.println("The area of the circle is " + area);
	}

}
