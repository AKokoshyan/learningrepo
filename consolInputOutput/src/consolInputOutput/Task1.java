package consolInputOutput;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		int firstNumber, secondNumber, thirdNumber;
		
		System.out.println("Enter three prime numbers and the program will compute the sum of them");
		
		Scanner scanner = new Scanner(System.in);
		
		firstNumber = scanner.nextInt();
		secondNumber = scanner.nextInt();
		thirdNumber = scanner.nextInt();

		int sum = firstNumber + secondNumber + thirdNumber;
		
		System.out.println("The result is " + sum);
	}

}
