package conditionalStatments;

import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		int firstNumber, secondNumber;

		System.out.println("Enter two numbers and the program will show the sign of the qutient ");

		Scanner scanner = new Scanner(System.in);
		firstNumber = scanner.nextInt();
		secondNumber = scanner.nextInt();

		int division = firstNumber / (secondNumber);

		if (division < 0) {
			System.out.println("The sign of the quotient is -");
		} else {
			System.out.println("The sign of the quotient is +");
		}
	}

}
