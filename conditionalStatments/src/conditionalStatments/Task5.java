package conditionalStatments;

import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		double a, b, c;

		System.out.println("The program will compute the radicals of the quadratic equation: ax^2 + bx + c = 0");

		Scanner scanner = new Scanner(System.in);

		a = scanner.nextInt();
		b = scanner.nextInt();
		c = scanner.nextInt();

		if (a == 0) {
			// a = 0!
			if (b != 0) {
				// a = 0!
				// b != 0
				double x = ((-1) * (c) / b);
				System.out.println("The equation have one radical " + x);
			} else {
				// a = 0!
				// b = 0!
				if (c == 0) {
					// a = 0
					// b = 0
					// c = 0
					System.out.println("The equation is true for every x ");
				} else {
					// a = 0
					// b = 0
					// c != 0
					System.out.println("The equation can not be satisfied for every x ");
				}
			}
		} else {
			// a != 0 (we know this already)
			double discriminant = Math.sqrt(Math.pow(b, 2) - 4 * a * c);

			if (discriminant > 0) {
				double radicalOne = ((-b + Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a));
				double radicalTwo = ((-b - Math.sqrt(Math.pow(b, 2) - 4 * a * c)) / (2 * a));
				System.out.println("There are two radicals x1 is " + radicalOne + " and x2 is " + radicalTwo);
			} else if (discriminant == 0) {
				double equalRadical = ((-b) / (2 * a));
				System.out.println("There is only one radical x1 = x2 " + equalRadical);
			} else {
				System.out.println("There are no real radicals");
			}
		}

	}

}
