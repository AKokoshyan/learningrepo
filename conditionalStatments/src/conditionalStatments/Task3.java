package conditionalStatments;

import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {
		int firstNumber, secondNumber, thirdNumber;

		System.out.println("Give tree numbers and the prgoram will show wiche is the greatest");

		Scanner scanner = new Scanner(System.in);

		firstNumber = scanner.nextInt();
		secondNumber = scanner.nextInt();
		thirdNumber = scanner.nextInt();

		boolean compare1 = (firstNumber > secondNumber && secondNumber > thirdNumber);
		boolean compare2 = (firstNumber < secondNumber && secondNumber > thirdNumber);

		if (compare1) {
			System.out.println("First number is the greatest");
		} else if (compare2) {
			System.out.println("Second number is the greatest");
		} else {
			System.out.println("Third number is the greatest");
		}
	}

}
