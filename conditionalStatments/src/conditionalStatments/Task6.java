package conditionalStatments;

import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		int firstNumber, secondNumber, thirdNumber, fourthNumber, fifthNumber;

		System.out.println("Enter five numbers and the program will give you back the greatest!");

		Scanner scanner = new Scanner(System.in);

		firstNumber = scanner.nextInt();
		secondNumber = scanner.nextInt();
		thirdNumber = scanner.nextInt();
		fourthNumber = scanner.nextInt();
		fifthNumber = scanner.nextInt();

		boolean firstTheGreatest = firstNumber > secondNumber && firstNumber > thirdNumber && firstNumber > fourthNumber
				&& firstNumber > fifthNumber;
		boolean secondTheGreatest = secondNumber > firstNumber && secondNumber > thirdNumber
				&& secondNumber > fourthNumber && secondNumber > fifthNumber;
		boolean thirdTheGreatest = thirdNumber > firstNumber && thirdNumber > secondNumber && thirdNumber > fourthNumber
				&& thirdNumber > fifthNumber;
		boolean fourthTheGreatest = fourthNumber > firstNumber && fourthNumber > secondNumber
				&& fourthNumber > thirdNumber && fourthNumber > fifthNumber;
		boolean fifthTheGreatest = fifthNumber > firstNumber && fifthNumber > secondNumber && fifthNumber > thirdNumber
				&& fifthNumber > fourthNumber;

		if (firstTheGreatest) {
			System.out.println("First number is the greatest !");
		} else if (secondTheGreatest) {
			System.out.println("Second number is the greatest !");
		} else if (thirdTheGreatest) {
			System.out.println("Third number is the greatest !");
		} else if (fourthTheGreatest) {
			System.out.println("Fourth number is the greatest !");
		} else {
			System.out.println("Fifth number is the greatest !");
		}

	}

}
