package conditionalStatments;

import java.util.Scanner;

public class Test {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		int full = Integer.parseInt(scanner.nextLine());
		int first = Integer.parseInt(scanner.nextLine());
		int second = Integer.parseInt(scanner.nextLine());
		double hours = Double.parseDouble(scanner.nextLine());

		double debitFirst = first * hours;
		double debitSecond = second * hours;

		double debitSum = (debitFirst + debitSecond);
		double percentFull;
		if (full >= debitSum) {
			percentFull = (debitSum / full) * 100;
			double pipeOneFull = (debitFirst / percentFull) * 10;
			double pipeTwoFull = (debitSecond / percentFull) * 10;

			System.out.println("The pool is " + (int) percentFull + "% full. Pipe 1: " + (int) pipeOneFull
					+ "%. Pipe 2: " + (int) pipeTwoFull + "%.");
		} else {
			double overflow = (debitSum - full);

			System.out.println("For " + hours + " hours the pool overflows with " + (int) overflow + "liters.");
		}

	}

}