package conditionalStatments;

import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		int firstNumber, secondNumber, thirdNumber;

		System.out.println("Enter tree numbers ");

		Scanner scanner = new Scanner(System.in);

		firstNumber = scanner.nextInt();
		secondNumber = scanner.nextInt();
		thirdNumber = scanner.nextInt();

		boolean condition1 = firstNumber + secondNumber == 0;
		boolean condition2 = secondNumber + thirdNumber == 0;
		boolean condition3 = firstNumber + thirdNumber == 0;
		boolean condition4 = firstNumber + secondNumber + thirdNumber == 0;

		if (condition1) {
			System.out.println("The sum of a + b is 0");
			if (condition2) {
				System.out.println("The sum of a + b and b + c is 0");
			} else {
				if (condition3) {
					System.out.println("The sum of a + b and a + c is 0");
				} else {
					if (condition4) {
						System.out.println("The sum of a + b +c = 0");
					}
				}
			}
		} else {
			System.out.println("The sum of all fucking numbers is not 0");
		}
	}

}
