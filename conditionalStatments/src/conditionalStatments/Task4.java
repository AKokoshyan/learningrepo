package conditionalStatments;

import java.util.Scanner;

public class Task4 {

	public static void main(String[] args) {
		int number;
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter one number ");
		number = scanner.nextInt();

		switch (number) {
		case 1:
			System.out.println("The number is one ");
			break;
		case 2:
			System.out.println("The number is two ");
			break;
		case 3:
			System.out.println("The number is tree ");
			break;
		case 4:
			System.out.println("The number is four ");
			break;
		case 5:
			System.out.println("The number is five ");
			break;
		case 6:
			System.out.println("The number is six ");
			break;
		case 7:
			System.out.println("The number is seven ");
			break;
		case 8:
			System.out.println("The number is eight ");
			break;
		case 9:
			System.out.println("The number is nine ");
			break;

		}
	}

}
