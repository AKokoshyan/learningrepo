import java.lang.reflect.Array;
import java.util.Arrays;

public class Task5 {

	public static void main(String[] args) {
		int[] numbers = { 5, 4, 2, 1, 3 };
		int[] returnedArray = findMax(numbers.length, numbers);
		for (int i = 0; i < returnedArray.length; i++) {
			System.out.println(returnedArray[i]);
		}
	}

	static int[] findMax(int N, int[] numbers) {
		Arrays.sort(numbers);
		int[] i = new int[2];
		i[0] = numbers[N - 2];
		i[1] = numbers[N - 1];
		return i;

	}
}
