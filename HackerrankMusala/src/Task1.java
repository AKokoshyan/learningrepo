import java.awt.List;
import java.util.ArrayList;

public class Task1 {
	public static void main(String[] args) {
		String s = "I am using Hackerrank to improve programing";
		String t = "am am am am Hackerrank to improve";

		String[] array = missingWords(s, t);
		
		
		for (int i = 0; i < array.length; i++) {
			System.out.println(array[i]);
		}

	}

	static String[] missingWords(String s, String t) {
		String[] sentence = s.split(" ");
		String[] subWords = t.split(" ");
		int lengthOfMissingWords = 0;

		for (int i = 0; i < sentence.length; i++) {
			boolean isNotMissing = false;
			for (int j = 0; j < subWords.length; j++) {
				if (sentence[i].equals(subWords[j])) {
					isNotMissing = true;
					break;
				}
			}

			if (!isNotMissing) {
				lengthOfMissingWords++;
			}
		}

		String[] missingWords = new String[lengthOfMissingWords];
		int currentIndex = 0;
		for (int i = 0; i < sentence.length; i++) {
			String currentWord = new String("");
			currentWord = sentence[i];
			boolean contains = false;
			for (int j = 0; j < subWords.length; j++) {
				if (subWords[j].equals(currentWord)) {
					contains = true;
					break;
				}
			}

			if (!contains) {
				missingWords[currentIndex] = currentWord;
				currentIndex++;
			}
		}

		return missingWords;

	}

}
