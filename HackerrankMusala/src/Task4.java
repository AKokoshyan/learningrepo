import java.util.ArrayList;
import java.util.List;

public class Task4 {
	public static void main(String[] args) {
		printNumbers();

	}

	static String[] printNumbers() {
		List<String> l = new ArrayList<>();
		for (int i = 1; i <= 100; i++) {
			String tmp = "";
			if (i % 3 == 0) {
				tmp = "Fizz";
			}
			if (i % 5 == 0) {
				tmp = "Buzz";
			}
			if (i % 15 == 0) {
				tmp = "FizzBuzz";
			}
			if (tmp.isEmpty()) {
				tmp = Integer.toString(i);
			}
			l.add(tmp);
		}
		String[] s = new String[l.size()];
		System.out.println(l);
		return l.toArray(s);
	}
}
