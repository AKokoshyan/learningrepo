import java.util.Scanner;

public class Task3Menu {
	/**
	 * Choose a option from the menu.If you press 1 you have to enter a number
	 * if you press 2 you exit from the menu and the program will tell you how
	 * many numbers had you entered. If you enter a different number the program
	 * will tell you that you entered wrong option from the menu try again *
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter option from the menu: ");

		int infinity = 100;
		int menuOption;
		int custemerNumbers;
		int counter = 0;
		int[] array = new int[infinity];
		do {
			System.out.println("To enter a number press 1 ");
			System.out.println("To exit from the menu press 2 ");

			menuOption = scanner.nextInt();

			if (menuOption < 1 || menuOption > 2) {
				System.out.println("You had entered an undefind option from the menu.");
			} else if (menuOption == 1) {
				System.out.println("Enter a number :");
				custemerNumbers = scanner.nextInt();
				array[counter] = custemerNumbers;
				counter++;
			}

		} while (menuOption != 2);

		for (int i = 0; i < counter; i++) {
			System.out.println("The numbers from the array are : " + array[i] + " ");
		}
	}

}
