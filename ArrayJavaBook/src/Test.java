import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayElements;
		do {
			System.out.println(
					"Enter how much nuber do you want to enter? The number must be positive or greater than 0");
			arrayElements = scanner.nextInt();
			if (arrayElements <= 0) {
				System.out.println(
						"You had entered negative number or 0! You have to enter positive number or greater than 0! Try again!");
			}
		} while (arrayElements <= 0);
		int[] firstArray = new int[arrayElements];
		System.out.println("Enter " + arrayElements + " numbers and program will print them in reverse!");
		for (int i = 0; i < firstArray.length; i++) {
			System.out.println("Enter " + (i + 1) + " number: ");
			firstArray[i] = scanner.nextInt();

		}
		for (int k = arrayElements - 1; k >= 0; k--) {
			if (k > 0) {
				System.out.print(firstArray[k] + ", ");
			} else {
				System.out.print(firstArray[k]);
			}
		}
	}

}
