import java.lang.reflect.Array;
import java.util.Scanner;

public class Task2 {
	/**
	 * �� �� ������ ��������, ����� ���� ��� ������ �� ��������� � ���������
	 * ���� �� �������
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Provide 2 numbers which will be later considered for 2 arrays length. Note: The length of an array should be positive.");
		int firstArrayLength;
		int secondArrayLength;

		do {
			System.out.println("Enter the first number: ");
			firstArrayLength = scanner.nextInt();
			if (firstArrayLength <= 0) {
				System.out.println(
						"Your first provided number is negative or equal to 0. You have to enter a positve number! Please, try again!");
			}
		} while (firstArrayLength <= 0);

		do {
			System.out.println("Enter the second number: ");
			secondArrayLength = scanner.nextInt();
			if (secondArrayLength <= 0) {
				System.out.println(
						"Your second number is negative or equal to 0. You have to enter a positve number! Please, try again!");
			}
		} while (secondArrayLength <= 0);

		int[] firstArray = new int[firstArrayLength];
		int[] secondArray = new int[secondArrayLength];

		if (firstArray.length == secondArray.length) {

			System.out.println("ARRAY 1:");
			for (int i = 0; i < firstArrayLength; i++) {
				System.out.println("Enter " + (i + 1) + " number: ");
				firstArray[i] = scanner.nextInt();
			}

			System.out.println("ARRAY 2:");
			for (int i = 0; i < secondArrayLength; i++) {
				System.out.println("Enter " + (i + 1) + " number: ");
				secondArray[i] = scanner.nextInt();
			}

			boolean isEqual = true; // isEqual = true;
			for (int i = 0; i < firstArrayLength; i++) {
				if (firstArray[i] != secondArray[i]) {
					isEqual = false;
					break;
				}
			}

			if (isEqual) {
				System.out.println("Two arrays are equal");
			} else {
				System.out.println("Two arrays are not equal");
			}

		} else {
			System.out.println("The arrays are with diffrent lenth. Arrays are not equal!");
		}

	}
}