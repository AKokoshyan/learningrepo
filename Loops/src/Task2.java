import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		System.out.println("Enter a number greater than 10 ");
		int variable;
		Scanner scanner = new Scanner(System.in);
		do {
			variable = scanner.nextInt();
			if (variable <= 10) {
				System.out.println("Sorry, the number that you entered is less then 10! ");
			}
		} while (variable <= 10);
		System.out.println("Congratulation, you entered number greater than 10 !");
	}

}
