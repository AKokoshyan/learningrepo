import java.util.Scanner;

public class Task5Fibonachi {

	public static void main(String[] args) {
		System.out.println("Enter number and the program will compute the sum of Fibonachi's row to you number");
		Scanner scanner = new Scanner(System.in);
		int clientNumber = scanner.nextInt();
		int firstFibonachi = 1;
		int nextFibonachi = 1;
		int fibonachiNumber;
		int sum = 0;

		for (int i = 0; i < clientNumber; i++) {
			fibonachiNumber = firstFibonachi + nextFibonachi;
			sum += firstFibonachi;
			firstFibonachi=nextFibonachi;
			nextFibonachi=fibonachiNumber;
		}
		System.out.println(sum);
	}

}
