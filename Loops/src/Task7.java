import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int k = scanner.nextInt();
		int numbFak = n - k;
		
		for (int i = n - 1; i > 0; i--) {
			n *= i;
		}
		for (int f = k - 1; f > 0; f--) {
			k *= f;
		}
		for (int h = numbFak - 1; h > 0; h--) {
			numbFak *= h;
		}
		System.out.println("N! * K!/(N-K)! = " + ((n * k) / numbFak));
	}

}
