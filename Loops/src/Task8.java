import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {
		System.out.println("Enter value for n and x ");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int x = scanner.nextInt();
		int temp = 1;
		int sum = 1;

		for (int i = 0; i < n; i++) {
			temp *= i / x;
			sum += temp;
		}
		System.out.println("The sum is " + sum);
	}

}
