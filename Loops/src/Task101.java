import java.util.Scanner;

public class Task101 {

	public static void main(String[] args) {
		System.out.println("Enter a number and the program will make the sum of all numbers ");
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();
		int sum = 0;

		for (int i = 0; i < number; i++) {
			sum += i;
		}
		System.out.println(sum);
	}

}
