import java.util.Scanner;

public class Task9 {

	public static void main(String[] args) {
		System.out.println("Write a program wiche exhibit the last number of Catan");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int fact2 = 2 * n;
		int factN = n + 1;

		for (int i = fact2 - 1; i > 0; i--) {
			fact2 *= i;
		}
		for (int f = factN - 1; f > 0; f--) {
			factN *= f;
		}
		for (int h = n - 1; h > 0; h--) {
			n *= h;
		}
		System.out.println("(2n)!/(n + 1)!*n! = " + (fact2 / (factN) * n));
	}

}
