import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		System.out.println("The program will compute the N!/K! Enter two integer numbers for N and K");
		Scanner scanner = new Scanner(System.in);

		int N = scanner.nextInt();
		int K = scanner.nextInt();

		if (1 < N && N < K) {
			for (int i = N - 1; i > 0; i--) {
				N *= i;
			}
			for (int i = K - 1; i > 0; i--) {
				K *= i;
			}
			N /= K;
			System.out.println("N! / K! = " + N);
		} else {
			System.out.println("The numbers that you entered are not correct");
		}

	}

}
