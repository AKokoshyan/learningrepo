import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();

		for (int i = 1; i <= n; i += 2) {
			if (i == n || i == n - 1) {
				System.out.print(i);
			} else {
				System.out.print(i + ", ");
			}
		}

	}
}