
public class Task4 {

	public static void main(String[] args) {
		int allCards = 13;
		int allCardsColor = 4;
		String currentColor = null;
		String currentNumberOfTheCard = null;
		for (int i = 1; i <= allCardsColor; i++) {
			switch (i) {
			case 1:
				currentColor = "diamond";
				break;
			case 2:
				currentColor = "bowl";
				break;
			case 3:
				currentColor = "club";
				break;
			case 4:
				currentColor = "spade";
				break;
			default:
				System.out.println("You had entered in default case first loop");
				break;
			}

			for (int j = 1; j <= allCards; j++) {

				switch (j) {
				case 1:
					currentNumberOfTheCard = "Ace";
					break;
				case 2:
					currentNumberOfTheCard = "2";
					break;
				case 3:
					currentNumberOfTheCard = "3";
					break;
				case 4:
					currentNumberOfTheCard = "4";
					break;
				case 5:
					currentNumberOfTheCard = "5";
					break;
				case 6:
					currentNumberOfTheCard = "6";
					break;
				case 7:
					currentNumberOfTheCard = "7";
					break;
				case 8:
					currentNumberOfTheCard = "8";
					break;
				case 9:
					currentNumberOfTheCard = "9";
					break;
				case 10:
					currentNumberOfTheCard = "10";
					break;
				case 11:
					currentNumberOfTheCard = "J";
					break;
				case 12:
					currentNumberOfTheCard = "Q";
					break;
				case 13:
					currentNumberOfTheCard = "K";
					break;
				default:
					System.out.println("You had entered in default case second loop");
					break;
				}

				if (currentNumberOfTheCard != null) {
					System.out.println(currentNumberOfTheCard + " " + currentColor);
				}

			}
		}
	}
}