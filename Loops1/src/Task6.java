import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		System.out.println(
				"Enter a value for N and K they must be greater than 1 and N must be greater than K. The program will compute the N!/K!");
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		int K = scanner.nextInt();
		int factorial = 1;
		int secondFactrorial = 1;

		if (K > 1 && N > K) {
			for (int i = 1; i <= N; i++) {
				factorial *= i;
			}
			for (int j = 1; j <= K; j++) {
				secondFactrorial *= j;

			}
			System.out.println("N!/K! = " + (factorial / secondFactrorial));
		} else {
			System.out.println("The numbers that you entered are not correct!");
		}
	}

}
