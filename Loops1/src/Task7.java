import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		System.out.println("Enter two numbers N and K and the program will compute N!*K!/(N-K)!");
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		int K = scanner.nextInt();
		int diffrenceBetweenThem = N - K;
		int factorial = 1;
		int secondFactorial = 1;
		int thirdFactorial = 1;

		for (int i = 1; i <= N; i++) {
			factorial *= i;
		}
		for (int j = 1; j <= K; j++) {
			secondFactorial *= j;
		}
		for (int h = 1; h <= diffrenceBetweenThem; h++) {
			thirdFactorial *= h;
		}
		System.out.println("N!*K!/(N-K)! = " + (factorial * secondFactorial / thirdFactorial));
	}

}
