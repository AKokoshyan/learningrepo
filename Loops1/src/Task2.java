import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		System.out.println(
				"Enter the number and the program will show the numbers in the row which can't be divided by 3 and 7");
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();

		for (int i = 0; i < custemerNumber; i++) {
			if (i % 3 != 0 && i % 7 != 0) {
				System.out.println("The numbers in the row are " + i);
			}
		}
	}

}
