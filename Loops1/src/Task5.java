import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		System.out.println(
				"Enter a number and the program will exhibit the sum of fibonachis number in a row of your number");
		Scanner scanner = new Scanner(System.in);
		int N = scanner.nextInt();
		int firstNumber = 1;
		int secondNumber = 1;
		int nextNumber;
		int sum = 0;

		for (int i = 0; i < N; i++) {
			nextNumber = firstNumber + secondNumber;
			sum +=firstNumber;
			firstNumber = secondNumber;
			secondNumber = nextNumber;
		}
		System.out.println("The sum of Fibonachis number is " + sum);
	}

}
