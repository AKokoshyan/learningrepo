import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {
		System.out.println("Enter a number and the program will exhibit the lowest ant highest number in the row");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int minNumber = 1;
		int maxNumber = Math.max(n, n);

		for (int i = 0; i <= n; i++) {
			if (i == minNumber) {
				System.out.println("The lowest number is " + i);
			}
			if (i == maxNumber) {
				System.out.println("The highest number is " + i);
			}
		}
	}

}
