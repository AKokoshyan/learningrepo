import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		System.out.println("Enter a number and the program will print all numbers of the row");
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int numbsOfTheRow = 0;

		for (int i = 0; i <= n; i++) {
			System.out.println(i);
		}

	}

}
