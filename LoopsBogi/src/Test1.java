import java.util.Scanner;

public class Test1 {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int x = 0, y = 0;
		System.out.print("Enter rows ");
		x = keyboard.nextInt();
		int forLoop = x;
		int curentNumber = forLoop;
		int divider;
		int constat = 10;
		for (int i = 1; i < x; i *= 10) {
			divider = curentNumber % 10;
			switch (divider) {
			case 1:
				System.out.println("one");
				break;
			case 2:
				System.out.println("two");
				break;
			case 3:
				System.out.println("tree");
				break;
			case 4:
				System.out.println("four");
				break;
			case 5:
				System.out.println("five");
				break;
			case 6:
				System.out.println("six");
				break;
			case 7:
				System.out.println("seven");
				break;
			case 8:
				System.out.println("eight");
				break;
			case 9:
				System.out.println("nine");
				break;
			}
			curentNumber = forLoop / constat;
			constat *= 10;
		}
	}
}