import java.util.Scanner;

/**
 * Write a program to calculate sum of digits of any number.
 * 
 * @author Tonio
 *
 */
public class Task7 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int newNumber = custemerNumber;
		int digit;
		int sumDigit = 0;

		while (newNumber > 0) {
			digit = newNumber % 10;
			newNumber = newNumber / 10;
			sumDigit += digit;
		}
		System.out.println(sumDigit);
	}

}
