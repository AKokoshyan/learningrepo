import java.util.Scanner;
/**
 * Write a program to find first and last digit of any number
 * @author Tonio
 *
 */

public class Task5 {
	public static void main(String[] args) {
		Scanner keyboard = new Scanner(System.in);
		int x = 0, y = 0;
		System.out.print("Enter rows ");
		x = keyboard.nextInt();
		for (int i = 1; i <= x; i++) {
			if (i == 1) {
				System.out.print(i % 10 + ", ");
			}
			if (i == x) {
				System.out.print(i % 10);
			}
		}
	}
}