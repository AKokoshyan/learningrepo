import java.util.Scanner;

/**
 * 19. Write a program to display the multiplication table of a given integer. -
 * ok Test Data : Input the number (Table to be calculated) : 15 Expected Output
 * : 15 X 1 = 15 ... ... 15 X 10 = 150
 * 
 * @author Tonio
 *
 */

public class Task13MultiplicationTable {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int result = 0;
		for (int i = 1; i <= 10; i++) {
			result = custemerNumber * i;

			System.out.println(custemerNumber + " x " + i + " = " + result);
		}
	}
}
