import java.util.Scanner;

public class Task28 {
	/**
	 * Write a program in C to display the pattern like a diamond
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber;
		do {
			System.out.println("Enter a positive number and greater than 0 and program will print a diamand ");
			custemerNumber = scanner.nextInt();
			if (custemerNumber <= 0) {
				System.out.println(
						"You had entered an unapropriate numer! The number must be positive and greater than 0! Try again! ");
			}
		} while (custemerNumber <= 0);

		int triangleSpace = custemerNumber;

		for (int i = 1; i <= custemerNumber; i++) {
			for (int j = triangleSpace; j > 0; j--) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*" + " ");
			}
			System.out.println();
			triangleSpace--;
		}
		int reverseTriangleSpace = custemerNumber - 1;
		for (int i = 2; i <= custemerNumber; i++) {
			for (int k = 1; k <= i; k++) {
				System.out.print(" ");
			}
			for (int j = reverseTriangleSpace; j > 0; j--) {
				System.out.print("*" + " ");
			}
			System.out.println();
			reverseTriangleSpace--;
		}
	}
}