import java.util.Scanner;

/**
 * 23. Write a program to make such a pattern like right angle triangle with
 * number increased by 1. The pattern like : 
 * 1
 * 2 3 
 * 4 5 6 
 * 7 8 9 10
 * 
 * @author Tonio
 *
 */

public class Task17 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int sum = 1;
		for (int i = 1; i <= custemerNumber; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(sum + " ");
				sum += 1;
			}
			System.out.println("");

		}

	}

}
