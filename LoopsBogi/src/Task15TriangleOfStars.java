import java.util.Scanner;

/**
 * 21. Write a program to display the pattern like right angle triangle using an
 * asterisk. -ok The pattern like :
 *
 **
 ****
 *****
 ****** 
 * @author Tonio
 *
 */

public class Task15TriangleOfStars {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		for (int i = 1; i <= custemerNumber; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print("*");
			}
			System.out.println("");
		}
	}

}
