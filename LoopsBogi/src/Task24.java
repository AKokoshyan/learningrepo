import java.util.Scanner;

/**
 * 25. Write a program to make such a pattern like a pyramid with an asterisk. 
 *		 1 
 *	    2 3 
 *     4 5 6 
 *    7 8 9 10
 * 
 * @author Tonio
 *
 */

public class Task24 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int blanSpace = custemerNumber;
		int curentNumberPiramid = 1;
		for (int i = 1; i <= custemerNumber; i++) {
			for (int j = blanSpace; j > 0; j--) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print(curentNumberPiramid + " ");
				curentNumberPiramid++;
			}
			System.out.println();
			blanSpace--;

		}
	}
}