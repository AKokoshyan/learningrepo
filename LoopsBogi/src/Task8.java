import java.util.Scanner;

/**
 * Write a program to enter any number and print all factors of the number.
 * 
 * @author Tonio
 *
 */

public class Task8 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int power = scanner.nextInt();
		int result = custemerNumber;

		for (int i = 2; i <= power; i++) {
			result *= custemerNumber;
		}
		System.out.println(result);
	}

}
