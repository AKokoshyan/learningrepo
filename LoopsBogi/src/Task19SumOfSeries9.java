import java.util.Scanner;

/**
 * 26. Write a program to display the sum of the series [ 9 + 99 + 999 + 9999
 * ...] Test Data : Input the number or terms :5 Expected Output : 9 99 999 9999
 * 99999 The sum of the saries = 111105
 * 
 * @author Tonio
 *
 */

public class Task19SumOfSeries9 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int constant = 10;

		int sum = 0;

		for (int i = 1; i <= custemerNumber; i++) {
			int currentNumberSerie = constant - 1;
			System.out.print(currentNumberSerie + " ");
			sum += currentNumberSerie;
			constant *= 10;
		}
		System.out.println("\n"+sum);
	}

}
