import java.util.Scanner;

public class Task4 {

	public static void main(String[] args) {
		System.out.println("Enter a number and the program will compute the sum of all odd numbers of the row");
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int sum = 0;

		for (int i = 1; i <= custemerNumber; i += 2) {
			sum += i;
		}
		System.out.println(sum);
	}

}
