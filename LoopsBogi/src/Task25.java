import java.util.Scanner;

/**
 * 25. Write a program to make such a pattern like a pyramid with an asterisk. 
 *		 * 
 *	    * * 
 *     * * * 
 *    * * * *
 * 
 * @author Tonio
 *
 */

public class Task25 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		int blankSpace = custemerNumber;
		for (int i = 1; i <= custemerNumber; i++) {
			for (int j = blankSpace; j > 0; j--) {
				System.out.print(" ");
			}
			for (int k = 1; k <= i; k++) {
				System.out.print("*" + " ");
			}
			System.out.println();
			blankSpace -= 1;
		}
	}
}