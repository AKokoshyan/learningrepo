import java.util.Scanner;

/**
 * 17. Write a program to read 10 numbers from keyboard and find their sum and
 * average. - ok
 * 
 * @author Tonio
 *
 */

public class Task171 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int numbersCount;
		int currentNumber;
		int sumOfAllNumbers = 0;
		do {
			System.out.println(
					"Enter positive number and the program will compute the sum of all numbers in the row and it will find average of them ");
			numbersCount = scanner.nextInt();
			if (numbersCount <= 0) {
				System.out.println("Try again!");
			}

		} while (numbersCount <= 0);

		for (int i = 1; i <= numbersCount; i++) {
			System.out.println("Enter " + i + " number: ");
			currentNumber = scanner.nextInt();
			sumOfAllNumbers += currentNumber;
		}

		double average = sumOfAllNumbers / numbersCount;
		System.out.println("The sum of all numbers is " + sumOfAllNumbers);
		System.out.println("The average of all numbers is " + average);
	}
}
