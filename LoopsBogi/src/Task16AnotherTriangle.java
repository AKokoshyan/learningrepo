import java.util.Scanner;

/**
 * 22. Write a program to make such a pattern like right angle triangle with a
 * number which will repeat a number in a row. - ok The pattern like : 
 * 1 
 * 22 
 * 333
 * 4444
 * @author Tonio
 */

public class Task16AnotherTriangle {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();
		
		for (int i = 1; i <= custemerNumber; i++) {
			for (int j = 0; j < i; j++) {
				System.out.print(i);
			}
			System.out.println("");
		}

	}
}