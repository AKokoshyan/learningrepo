import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		System.out.println(salutTheCustemer("Peter"));
		testSalutTheCustomer();
	}

	public static String salutTheCustemer(String name) {
		String result = "Hello, " + name;
		return result;
	}

	public static void testSalutTheCustomer() {

		String actualResult = salutTheCustemer("Gosho");
		String expectedResult = "Hello, Peter";

		if (expectedResult.equals(actualResult)) {
			System.out.println("testSalutTheCustomer() is valid!");
		} else {
			System.out.println("testSalutTheCustomer() is not valid.\nExpected result is: \"" + expectedResult
					+ "\", but the actual result was: \"" + actualResult + "\"");
		}
	}
}
