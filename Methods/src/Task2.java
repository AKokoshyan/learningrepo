import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter tree numbers and the program will compute whiche one is the greatest!");

		System.out.println("Enter first number : ");
		int firstNumber = scanner.nextInt();

		System.out.println("Enter second number : ");
		int secondNumber = scanner.nextInt();

		System.out.println("Enter third number : ");
		int thirdNumber = scanner.nextInt();

		int compareFirstAndSecondNuber = getGreaterNumber(firstNumber, secondNumber);
		int compareSecondAndThirdNumber = getGreaterNumber(secondNumber, thirdNumber);
		int theGreatestNumber = getGreaterNumber(compareFirstAndSecondNuber, compareSecondAndThirdNumber);

		System.out.println("The greatest number is : " + theGreatestNumber);

		testgetGreaterNumber();

	}

	public static int getGreaterNumber(int x, int y) {
		if (x > y) {
			return x;
		} else {
			return y;
		}
	}

	public static void testgetGreaterNumber() {
		int greatestNumber = getGreaterNumber(5, 2);
		int expectedResult = 5;

		if (greatestNumber == expectedResult) {
			System.out.println("testgetGreaterNumber () is valid");
		} else {
			System.out.println("testgetGreaterNumber () is not valid! \nExpected result is " + expectedResult
					+ " and the actual result was " + greatestNumber);
		}
	}
}
