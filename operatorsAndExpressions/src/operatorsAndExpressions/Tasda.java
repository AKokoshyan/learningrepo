package operatorsAndExpressions;

import java.util.Scanner;

public class Tasda {

	public static void main(String[] args) {
		double a;
		double b;
		System.out.println("This program will compute the surface and area of rectangle. Give value for a and b ");
		Scanner scanner = new Scanner(System.in);
		a = scanner.nextDouble();
		Scanner scanner1 = new Scanner(System.in);
		b = (int) scanner1.nextDouble();
		System.out.println("The result for surface is " + (a*b) + " and for area is " + 2*(a+b));
	}

}
