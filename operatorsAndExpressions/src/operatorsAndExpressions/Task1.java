package operatorsAndExpressions;

import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		int x;
		System.out.println("Write one number and I will tell if it is odd or even");

		Scanner scanner = new Scanner(System.in);

		x = scanner.nextInt();

		if ((x % 2) == 0) {
			System.out.println("The number is even!");
		}

		System.out.println("Now checking if odd");
		if ((x % 2) == 1) {
			System.out.println("The number is odd!");
		}
		System.out.println("END");
	}

}
