package operatorsAndExpressions;

import java.util.Scanner;

public class Task9 {

	public static void main(String[] args) {
		double firstCoordinate;
		double secondCoordinate;
		double radiusCircle = 2.23;

		System.out.println("Give two values for coordinates to check are they in the circle");

		Scanner scanner = new Scanner(System.in);
		firstCoordinate = scanner.nextDouble();
		secondCoordinate = scanner.nextDouble();
		//From the coordinates we make one triangle and we are finding the hypotenuse 
		double pitagorTeoremy = (firstCoordinate*firstCoordinate+secondCoordinate*secondCoordinate);
		double c = Math.sqrt(pitagorTeoremy);
		
		//Here we compare the hypotenuse with the radius of the circle
		if (c<radiusCircle) {
			System.out.println("The coordinates are in the circle!");
		} else {
			System.out.println("The coordinates are out of the circle!");
		}

	}

}
