package operatorsAndExpressions;

import java.util.Scanner;

public class ShowTwoNames {

	public static void main(String[] args) {
		String firstName;
		String secondName;

		System.out.println("What is your first name?");

		Scanner scanner = new Scanner(System.in);
		firstName = scanner.nextLine();

		System.out.println("What is your second name?");
		secondName = scanner.nextLine();
		
		System.out.println("Hello, so your fullname is " + firstName+ " " + secondName);
	}

}
