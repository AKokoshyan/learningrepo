package operatorsAndExpressions;

import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		double personWeightInKilos;
		// Percentage of moon gravity compared to earth's
		double moonGravity = 0.17;
		
		System.out.println(
				"This program will compute how much will be your weight on the moon. What is your weiht in kg");
		
		Scanner scanner = new Scanner(System.in);
		personWeightInKilos = scanner.nextDouble();

		double personWeightOnMoon = personWeightInKilos * moonGravity;

		System.out.println("Your weight on the moon is " + personWeightOnMoon);
	}

}
