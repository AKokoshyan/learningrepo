package operatorsAndExpressions;

import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		double a;
		double b;
		double h;
		System.out.println("This program will compute the area of trepze. Give value for a, b and h ");
		Scanner scanner = new Scanner(System.in);

		a = scanner.nextDouble();
		b = scanner.nextDouble();
		h = scanner.nextDouble();
		
		double areaOfTrapze = ((a + b) * h) / 2;
		
		System.out.println("The area of trepze is " + areaOfTrapze );

	}

}
