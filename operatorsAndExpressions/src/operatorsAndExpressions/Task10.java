package operatorsAndExpressions;

import java.util.Scanner;

public class Task10 {

	public static void main(String[] args) {
		int abcd;
		System.out.println("Enter 4 digit number");
		Scanner scanner = new Scanner(System.in);
		abcd = scanner.nextInt();
		int a = abcd / 1000;
		int b = (abcd / 100) % 10;
		int c = (abcd / 10) % 10;
		int d = abcd % 10;
		int sum = a + b + c + d;
		System.out.println("The sum of the numbers is " + sum);
		System.out.println("The reverse order of the number is " + d + c + b + a);
		System.out.println("First number to be last one " + d + a + b + c);
		System.out.println("Second and third number to be exchaged " + a + c + b + d);
	}

}
