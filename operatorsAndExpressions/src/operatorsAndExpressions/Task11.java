package operatorsAndExpressions;

import java.util.Scanner;

public class Task11 {

	public static void main(String[] args) {
		double firstCoordinate;
		double secondCoordinate;
		double radiusCircle = 5;
		
		System.out.println("Give two values for the coordinates ");
		Scanner scanner = new Scanner(System.in);
		firstCoordinate = scanner.nextDouble();
		secondCoordinate = scanner.nextDouble();
		
		double hypotenusePowTwo = Math.pow(firstCoordinate, 2) + Math.pow(secondCoordinate, 2);
		double hypotenuse = Math.sqrt(hypotenusePowTwo);

		boolean isInCircle = hypotenuse < radiusCircle;
		boolean isFirstCoordinateOutOfRect = firstCoordinate < -1 || firstCoordinate > 5;
		boolean isSecondCoordinateOutOfRect = secondCoordinate < 1 || secondCoordinate > 5;
		boolean isOutOfRectangle = isFirstCoordinateOutOfRect && isSecondCoordinateOutOfRect;

		if (isInCircle && isOutOfRectangle) {
			System.out.println("The coordinates are in the circle and out of the rectangle!");
		} else {
			System.out.println("The coordinates are not in the circle!");
		}
	}

}
