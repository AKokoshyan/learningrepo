package operatorsAndExpressions;

import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {
		double x;
		double y;
		System.out.println("The circle is with points (0.0;5).Enter two numbers to check are they in the circle");
		Scanner scanner = new Scanner(System.in);
		x = scanner.nextInt();
		y = scanner.nextInt();
		double z = (x * x + y * y);
		double c = Math.sqrt(z);
		if (c < 5) {
			System.out.println("The point is in the circle!");
		} else {
			System.out.println("The point is out of the circle!");
		}
	}

}
