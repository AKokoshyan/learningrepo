package operatorsAndExpressions;

import java.util.Scanner;

public class ReadALetter {

	public static void main(String[] args) {

		char letter;

		Scanner scanner = new Scanner(System.in);

		System.out.println("What is your first latter from the name?");

		// Reading the first letter from the standard input stream
		letter = scanner.findInLine(".").charAt(0);

		System.out.println("My first letter is " + letter);
	}

}
