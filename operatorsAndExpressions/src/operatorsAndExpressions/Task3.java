package operatorsAndExpressions;

import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {
		int x;
		System.out.println("Write a number");
		Scanner scanner = new Scanner(System.in);
		x = scanner.nextInt();

		if (((x % 5) == 0) && ((x % 7) == 0)) {
			System.out.println("There is no remaining after dividing");
		}

		if (((x % 5) != 0) && ((x % 7) != 0)) {
			System.out.println("There is remaining after dividing");
		}
	}

}
