package operatorsAndExpressions;

import java.util.Scanner;

//Rear two integers from the standard input stream and output the sum of them.
public class SumTwoNumbers {
	public static void main(String[] arguments) {
		int first;
		int second;

		System.out.println("Your first number?");
		Scanner scanner = new Scanner(System.in);

		first = scanner.nextInt();

		System.out.println("Your second number?");

		second = scanner.nextInt();

		int sum = first + second;

		System.out.println("The sum of them is: " + sum);

	}

}
