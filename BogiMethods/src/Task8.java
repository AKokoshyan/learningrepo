import java.util.Scanner;

public class Task8 {
	/**
	 * Write a program in C to get the largest element of an array using the
	 * function.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int lengthArray = scanner.nextInt();
		int[] array = new int[lengthArray];

		for (int i = 0; i < lengthArray; i++) {
			array[i] = scanner.nextInt();
		}

		int greatestNumber = checkGreatestElement(array);

		System.out.println(greatestNumber);
	}

	public static int checkGreatestElement(int[] array) {
		int greatestNumber = array[0];
		for (int i = 0; i < array.length; i++) {
			if (greatestNumber < array[i]) {
				greatestNumber = array[i];
			}
		}
		return greatestNumber;
	}
}
