
public class Task5 {
	/**
	 * Write a program in C to find the sum of the series
	 * 1!/1+2!/2+3!/3+4!/4+5!/5 using the function
	 * 
	 * @param args
	 */

	public static int getSumOfSeries(int x) {
		int factorial = 1;
		int sumOfSeries = 1;
		for (int i = 1; i < x; i++) {
			factorial *= i;
			sumOfSeries += factorial;
		}
		return sumOfSeries;
	}

	public static void main(String[] args) {
		System.out.println("The program will find the sum of the series 1!/1+2!/2+3!/3+4!/4+5!/5 ");

		int maxNumber = 5;
		int sumOfSeries = getSumOfSeries(maxNumber);

		System.out.println("The result is " + sumOfSeries);

	}

}
