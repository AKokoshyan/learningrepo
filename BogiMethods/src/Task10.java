import java.util.Scanner;

public class Task10 {
	/**
	 * Write a program in C to print all perfect numbers in given range using
	 * the function
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int customerNumber = scanner.nextInt();

		printSumOfPerfectNumbers(customerNumber);
		printPerfectNumbers(customerNumber);
	}

	public static void printSumOfPerfectNumbers(int number) {
		int sum = 0;
		for (int i = 1; i < number; i++) {
			if (isPerfect(i)) {
				sum += i;
			}
		}

		System.out.println("Sum of perfect numbers of " + number + " = " + sum);
	}

	public static void printPerfectNumbers(int number) {
		for (int i = 1; i <= number; i++) {
			if (isPerfect(i)) {
				System.out.println(i);
			}
		}
	}

	public static boolean isPerfect(int number) {
		int sumOfDevidersOfNumber = 0;
		for (int i = 1; i < number; i++) {
			if (number % i == 0) {
				sumOfDevidersOfNumber += i;
			}
		}

		if (sumOfDevidersOfNumber == number) {
			return true;
		} else {
			return false;
		}
	}
}