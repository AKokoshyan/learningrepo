import java.util.Scanner;

public class Task7 {
	/**
	 * Write a program in C to check whether a number is a prime number or not
	 * using the function
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();

		checkNumberIsPrime(custemerNumber);
	}

	public static void checkNumberIsPrime(int prime) {
		boolean isPrime = true;

		if (prime == 1 || prime == 2) {
			isPrime = true;

		} else if (prime % 2 != 0) {

			for (int i = 3; i < prime; i++) {

				isPrime = true;

				if (prime % i == 0) {
					isPrime = false;
					break;
				}
			}
		} else {
			isPrime = false;
		}

		if (isPrime) {
			System.out.println("The number is prime");
		} else {
			System.out.println("The number is not prime");
		}

	}
}
