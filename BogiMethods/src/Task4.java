import java.util.Scanner;

public class Task4 {
	/**
	 * Write a program in C to check a given number is even or odd using the
	 * 
	 * @param args
	 */
	
	// The method below is checking the variable from main method is odd or even!
	public static void checkOddEven(int custemerNumber) {
		if (custemerNumber % 2 == 0) {
			System.out.println("The number that you entered is even");
		} else {
			System.out.println("The number that you entered is odd");
		}
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a integer number and the program will prin is it odd or even!");

		int custemerNumber = scanner.nextInt();

		checkOddEven(custemerNumber);
	}

}
