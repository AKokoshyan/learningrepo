
public class Task2 {
	/**
	 * Write a program in C to find the square of any number using the function.
	 * 
	 * @param args
	 */

	// The purpose of the method below is to get the square of the variable in
	// the main method!
	public static double getSquareConstantNumber(double x) {
		double square = x * x;
		return square;
	}

	public static void main(String[] args) {
		double constantNumber = 20;
		double squareOfTheNumber = getSquareConstantNumber(constantNumber);

		System.out.println("The square of " + constantNumber + " is : " + squareOfTheNumber);
	}

}
