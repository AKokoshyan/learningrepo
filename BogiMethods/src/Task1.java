
public class Task1 {
	/**
	 * Write a program in C to show the simple structure of a function
	 * 
	 * @param args
	 */

	// The purpose of the method below is to make sum of the variables in the
	// main
	// method!
	public static int getSum(int x, int y) {
		int sum = x + y;
		return sum;
	}

	public static void main(String[] args) {
		int firstNumber = 5;
		int secondNumber = 6;

		System.out.println("The total is : " + getSum(firstNumber, secondNumber));
	}

}
