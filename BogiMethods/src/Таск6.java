import java.util.Scanner;

public class ����6 {
	/**
	 * Write a program in C to convert decimal number to binary number using the
	 * function
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a decimal number and the program will convert it in binary ");

		int decimalNumber = scanner.nextInt();

		int binaryNumber = convertToBinary(decimalNumber);

		System.out.println("The number in binary system is : " + binaryNumber);

	}

	public static int convertToBinary(int decimalNumber) {
		int binaryNumber = 0;
		int elementOfBinaryRow = 0;
		int lengthOfBinRow = 1;

		while (decimalNumber != 0) {
			elementOfBinaryRow = decimalNumber % 2;
			binaryNumber = binaryNumber + elementOfBinaryRow * lengthOfBinRow;
			lengthOfBinRow *= 10;
			decimalNumber /= 2;
		}
		return binaryNumber;
	}

}
