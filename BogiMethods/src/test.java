import java.util.Scanner;

public class test {
	/**
	 * Write a program in C to get the largest element of an array using the
	 * function.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);

		System.out.println("Enter a number to declare an array the number must be positive and greater than 0");
		int lengthArray = scanner.nextInt();
		validation(lengthArray);

		int[] array = new int[lengthArray];

		for (int i = 0; i < lengthArray; i++) {
			array[i] = scanner.nextInt();
		}

		int greatestNumber = checkGreatestElement(array);

		System.out.println(greatestNumber);
	}

	public static int checkGreatestElement(int[] array) {
		int greatestNumber = array[0];
		for (int i = 0; i < array.length; i++) {
			if (greatestNumber < array[i]) {
				greatestNumber = array[i];
			}
		}
		return greatestNumber;
	}

	public static boolean validation(int custemerNumber) {
		boolean isValid;
		do {
			if (custemerNumber <= 0) {
				System.out.println(
						"The number that you entered is negative or equal to 0! It must be positive or greater than 0!! Try again!!!");
			}
			isValid = false;
		} while (custemerNumber <= 0);
		isValid = true;
		return isValid;
	}
}
