import java.util.Scanner;

public class Task9 {
	/**
	 * Write a program in C to check Armstrong and perfect numbers using the
	 * function
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int costemerNumber = scanner.nextInt();

		boolean armstrongNumber = checkNumberArmstrong(costemerNumber);
		boolean perfectNumber = checkNumberPerfect(costemerNumber);

		printTheResult(costemerNumber, armstrongNumber, perfectNumber);
	}

	// The method below is checking is the customer number an Armstrong number!
	public static boolean checkNumberArmstrong(int costemerNumber) {
		boolean isArmstrongNumber = false;
		int repeater = costemerNumber;
		int costemerNumberLengthPow = 0;
		int divider = 1;

		while (repeater > 0) {
			repeater /= divider;
			divider *= 10;
			costemerNumberLengthPow++;
		}

		int secondRepeater = costemerNumber;
		int currentElementOFTheRow;
		int secondDivider = 10;
		int sum = 0;

		for (int i = 0; i < costemerNumberLengthPow; i++) {
			currentElementOFTheRow = secondRepeater % 10;
			secondRepeater /= secondDivider;
			sum += Math.pow(currentElementOFTheRow, costemerNumberLengthPow);
		}
		if (sum == costemerNumber) {
			isArmstrongNumber = true;
		}
		return isArmstrongNumber;
	}

	// The method below is checking is the customer number a perfect number!
	public static boolean checkNumberPerfect(int costemerNumber) {
		boolean isPerfect = false;

		int sum = 0;

		for (int i = 1; i < costemerNumber; i++) {
			if (costemerNumber % i == 0) {
				sum += i;
			}
		}

		if (sum == costemerNumber) {
			isPerfect = true;
		}
		return isPerfect;
	}

	// The method below is printing the result in the main method!
	public static void printTheResult(int x, boolean armstrong, boolean perfect) {
		if (armstrong) {
			System.out.println("The " + x + " is an Armstrong number.");
		} else {
			System.out.println("The " + x + " is not an Armstrong number.");
		}
		if (perfect) {
			System.out.println("The " + x + " is a Perfect number.");
		} else {
			System.out.println("The " + x + " is not a Perfect number.");
		}
	}
}
