import java.util.Scanner;

public class Task8 {
	/**
	 * odd/even sum
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int custemerNumber = scanner.nextInt();

		int oddIndexSum = 0;
		int evenIndexSum = 0;
		int currentNumber;

		for (int i = 1; i <= custemerNumber; i++) {
			currentNumber = scanner.nextInt();
			if (i % 2 == 0) {
				evenIndexSum += currentNumber;
			} else if (i % 2 != 0) {
				oddIndexSum += currentNumber;
			}
		}

		int diffrence = oddIndexSum - evenIndexSum;
		if (evenIndexSum == oddIndexSum) {
			System.out.println("Yes sum " + oddIndexSum);
		} else {
			System.out.println("No diff " + Math.abs(diffrence));
		}
	}

}
