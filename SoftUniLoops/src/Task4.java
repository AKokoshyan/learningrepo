import java.util.Scanner;

public class Task4 {
	/**
	 * sum of number
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int n = scanner.nextInt();
		int currentNumber;
		int sum = 0;

		for (int i = 0; i < n; i++) {
			currentNumber = scanner.nextInt();
			sum += currentNumber;
		}

		System.out.println(sum);

	}

}
