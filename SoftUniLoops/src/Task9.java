import java.util.Scanner;

import javax.swing.plaf.IconUIResource;

public class Task9 {
	/**
	 * Vowels Sum
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String custemerText = scanner.nextLine();

		int sum = 0;

		for (int i = 0; i < custemerText.length(); i++) {
			String symbol = custemerText.charAt(i) + "";

			switch (symbol) {
			case "a":
				sum += 1;
				break;
			case "e":
				sum += 2;
				break;
			case "i":
				sum += 3;
				break;
			case "o":
				sum += 4;
				break;
			case "u":
				sum += 5;
				break;
			default:
				break;

			}
		}
		System.out.println(sum);
	}

}
