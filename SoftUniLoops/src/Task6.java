import java.util.Scanner;

public class Task6 {
	/**
	 * smallest number
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int inputNumber = scanner.nextInt();
		int minNumber = scanner.nextInt();
		int currentNumber;

		for (int i = 1; i <= inputNumber - 1; i++) {
			currentNumber = scanner.nextInt();
			if (currentNumber < minNumber) {
				minNumber = currentNumber;
			}
		}

		System.out.println(minNumber);

	}

}
