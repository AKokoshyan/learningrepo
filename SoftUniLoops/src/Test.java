import java.util.Scanner;

public class Test {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int customerNumber = scanner.nextInt();

		for (int i = 0; i < customerNumber; i++) {
			if (i == 0 || i == customerNumber - 1) {
				for (int j = 0; j < customerNumber; j++) {
					if (j == 0 || j == customerNumber - 1) {
						System.out.print("+");
					} else if (j % 2 != 0) {
						System.out.print(" ");
					} else {
						System.out.println("-");
					}

				}
			} else if (i % 2 == 0) {
				for (int j = 0; j < customerNumber; j++) {
					if (j == 0 || j == customerNumber - 1) {
						System.out.println("|");
					} else if (j % 2 != 0) {
						System.out.println(" ");
					} else {
						System.out.println("-");
					}
				}
			} else {
				System.out.println();
			}

		}
	}
}
