import java.util.Scanner;

public class Task4 {

	public static void main(String[] args) {
		System.out.println("Choose with the number from 1 to 5 what drink do you want.");
		Scanner scanner = new Scanner(System.in);
		int number = scanner.nextInt();

		switch (number) {
		case 1:
			System.out.println("Cola");
			break;
		case 2:
			System.out.println("Fanta");
			break;
		case 3:
			System.out.println("Fanta lemon");
			break;
		case 4:
			System.out.println("Fanta menZis");
			break;
		case 5:
			System.out.println("Sprite");
			break;
		default: // Conventional structure of "Switch case". It include
					// "default" and it's good to be at the end of the "switch
					// case" and must have "break" The body of "default" is
					// including the all other case which are not included in
					// the "switch case"
			System.out.println("Error. choice was not valid, here is your money back");
			break;
		}
	}
}
// Checked by Bogi
