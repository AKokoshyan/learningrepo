import java.util.Scanner;

public class Task1 {

	public static void main(String[] args) {
		System.out.println("Given an integer, and the program will perform the following conditional actions:");
		System.out.println("If n is odd, print Weird");
		System.out.println("If n is even and in the inclusive range of 2 to 5, print Not Weird");
		System.out.println("If n is even and in the inclusive range of  6 to 20, print Weird");
		System.out.println("If n  is even and greater than 20, print Not Weird");

		int theNumber;

		Scanner scanner = new Scanner(System.in);

		theNumber = scanner.nextInt();

		if (theNumber % 2 != 0) {
			System.out.println("Weird!");

		} else if (theNumber % 2 == 0 && theNumber >= 2 && theNumber <= 5) {
			System.out.println("Not weird!");
		} else if (theNumber % 2 == 0 && theNumber >= 6 && theNumber <= 20) {
			System.out.println("Weird!");
		} else if (theNumber % 2 == 0 && theNumber > 20) {
			System.out.println("Not Weird!");
		} else {
			System.out.println("The answer is out of the task's requirments!");
		}
	}

}
