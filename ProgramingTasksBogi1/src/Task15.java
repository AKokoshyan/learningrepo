import java.util.Scanner;

public class Task15 {

	public static void main(String[] args) {
		System.out.println(
				"The program will read any Month Number in integer from 1 to 12  and display the number of days for this month.");

		Scanner scanner = new Scanner(System.in);

		int monthNumber = scanner.nextInt();

		switch (monthNumber) {
		case 1:
			System.out.println("January have 31 days");
			break;
		case 2:
			System.out.println("February have 28 or 29 when the year is leap");
			break;
		case 3:
			System.out.println("March have 31 days");
			break;
		case 4:
			System.out.println("April have 30 days");
			break;
		case 5:
			System.out.println("May have 31 days");
			break;
		case 6:
			System.out.println("June have 30 days");
			break;
		case 7:
			System.out.println("July have 31 days");
			break;
		case 8:
			System.out.println("August have 31 days");
			break;
		case 9:
			System.out.println("September have 30 days");
			break;
		case 10:
			System.out.println("October have 31 days");
			break;
		case 11:
			System.out.println("November have 30 days");
			break;
		case 12:
			System.out.println("December have 31 days");
			break;
		default:
			System.out.println("The number that you entered is not equal to a month!");
			break;

		}

	}

}
