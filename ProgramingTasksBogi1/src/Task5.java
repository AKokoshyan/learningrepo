import java.util.Scanner;

public class Task5 {

	public static void main(String[] args) {
		System.out.println("Write a program to accept two integers and check whether they are equal or not!");
		
		Scanner scanner = new Scanner(System.in);
		
		int firstVariable = scanner.nextInt();
		int secondVariable = scanner.nextInt();
		
		if (firstVariable == secondVariable){
			System.out.println("The two integers are equal!");
		} else {
			System.out.println("The two integers are not equal!");
		}

	}

}
//Checked by Bogi