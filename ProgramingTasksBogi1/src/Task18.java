import java.util.Scanner;

public class Task18 {

	public static void main(String[] args) {
		System.out.println("Enter your month and day of birth and the program will give you your zodiacal sign");

		Scanner scanner = new Scanner(System.in);

		int usersMonth = scanner.nextInt();
		int usersBirthday = scanner.nextInt();

		if (usersMonth >= 0 && usersMonth <= 13 && usersBirthday >= 0 && usersBirthday <= 31) {
			if (usersMonth == 12 && (usersBirthday >= 22 && usersBirthday <= 31)
					|| usersMonth == 1 && (usersBirthday >= 1 && usersBirthday <= 19)) {
				System.out.println("Your zodiacal sign is Capricorn");
			} else if (usersMonth == 1 && (usersBirthday >= 20 && usersBirthday <= 31)
					|| usersMonth == 2 && (usersBirthday >= 1 && usersBirthday <= 18)) {
				System.out.println("Your zodiacal sign is Aquarius ");
			} else if (usersMonth == 2 && (usersBirthday >= 19 && usersBirthday <= 29)
					|| usersMonth == 3 && (usersBirthday >= 1 && usersBirthday <= 20)) {
				System.out.println("Your zodiacal sign is Pisces ");
			} else if (usersMonth == 3 && (usersBirthday >= 21 && usersBirthday <= 31)
					|| usersMonth == 4 && (usersBirthday >= 1 && usersBirthday <= 19)) {
				System.out.println("Your zodiacal sign is Aries ");
			} else if (usersMonth == 4 && (usersBirthday >= 20 && usersBirthday <= 30)
					|| usersMonth == 5 && (usersBirthday >= 1 && usersBirthday <= 20)) {
				System.out.println("Your zodiacal sign is Taurus ");
			} else if (usersMonth == 5 && (usersBirthday >= 21 && usersBirthday <= 31)
					|| usersMonth == 6 && (usersBirthday >= 1 && usersBirthday <= 20)) {
				System.out.println("Your zodiacal sign is Gemini");
			} else if (usersMonth == 6 && (usersBirthday >= 21 && usersBirthday <= 30)
					|| usersMonth == 7 && (usersBirthday >= 1 && usersBirthday <= 22)) {
				System.out.println("Your zodiacal sign is Cancer");
			} else if (usersMonth == 7 && (usersBirthday >= 23 && usersBirthday <= 31)
					|| usersMonth == 8 && (usersBirthday >= 1 && usersBirthday <= 22)) {
				System.out.println("Your zodiacal sign is Leo");
			} else if (usersMonth == 8 && (usersBirthday >= 23 && usersBirthday <= 31)
					|| usersMonth == 9 && (usersBirthday >= 1 && usersBirthday <= 22)) {
				System.out.println("Your zodiacal sign is Virgo");
			} else if (usersMonth == 9 && (usersBirthday >= 21 && usersBirthday <= 30)
					|| usersMonth == 10 && (usersBirthday >= 1 && usersBirthday <= 22)) {
				System.out.println("Your zodiacal sign is Libra");
			} else if (usersMonth == 10 && (usersBirthday >= 23 && usersBirthday <= 31)
					|| usersMonth == 11 && (usersBirthday >= 1 && usersBirthday <= 21)) {
				System.out.println("Your zodiacal sign is Scorpio");
			} else if (usersMonth == 11 && (usersBirthday >= 22 && usersBirthday <= 30)
					|| usersMonth == 12 && (usersBirthday >= 1 && usersBirthday <= 21)) {
				System.out.println("Your zodiacal sign is Sagittarius");
			}
		} else {
			System.out.println("The month or birthday that you entered is not correct");
		}

	}

}
