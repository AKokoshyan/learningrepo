import java.util.Scanner;

public class Task12 {

	public static void main(String[] args) {
		System.out.println("type charecter");
		Scanner scanner = new Scanner(System.in);

		char symbol = scanner.nextLine().charAt(0);

		if ((symbol >= 32 && symbol <= 47) || (symbol >= 58 && symbol <= 64) || (symbol >= 91 && symbol <= 96)
				|| (symbol >= 123 && symbol <= 127)) {
			System.out.println("The symbol that you entered is special");
		} else if (symbol >= 65 && symbol <= 120) {
			System.out.println("The symbol that you entered is from the alphabet");
		} else {
			System.out.println("The symbol that you entered is digit");
		}

	}

}
