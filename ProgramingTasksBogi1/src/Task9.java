import java.util.Scanner;

public class Task9 {

	public static void main(String[] args) {
		System.out.println(
				"The program will read temperature in celsium and display a suitable message according to temperature state below :");
		System.out.println("Temp < 0 then Freezing weather");
		System.out.println("Temp 0-10 then Very Cold weather");
		System.out.println("Temp 10-20 then Cold weather");
		System.out.println("Temp 20-30 then Normal in Temp");
		System.out.println("Temp 30-40 then Its Hot");
		System.out.println("Temp >=40 then Its Very Hot");

		Scanner scanner = new Scanner(System.in);
		double outsideTemperature = scanner.nextDouble();

		if (outsideTemperature < 0) {
			System.out.println("Freezing weather");
		} else if (outsideTemperature >= 0 && outsideTemperature <= 10) {
			System.out.println("Very Cold weather");
		} else if (outsideTemperature > 10 && outsideTemperature <= 20) {
			System.out.println("Cold weather");
		} else if (outsideTemperature > 20 && outsideTemperature <= 30) {
			System.out.println("Normal in Temp");
		} else if (outsideTemperature > 30 && outsideTemperature <= 40) {
			System.out.println("Its Hot");
		} else {
			System.out.println("Its Very Hot");
		}
	}
}
