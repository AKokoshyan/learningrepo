import java.util.Scanner;

public class Task7 {

	public static void main(String[] args) {
		System.out.println(
				"Write a program to read the value of an integer m and display the value of m is 1 when m is larger than 0, 0 when m is 0 and -1 when m is less than 0");

		Scanner scanner = new Scanner(System.in);
		int mValue = scanner.nextInt();
		
		if (mValue > 1){
			System.out.println("m is 1");
		}else if (mValue == 0){
			System.out.println(" m is 0");
		}else {
			System.out.println("m is -1");
		}
	}

}
// Checked by Bogi 