import java.util.Scanner;

public class Task8 {

	public static void main(String[] args) {
		System.out.println(
				"Write a program to accept a coordinate point in a XY coordinate system and determine in which quadrant the coordinate point lies.");

		Scanner scanner = new Scanner(System.in);

		int abscissaCoordinate = scanner.nextInt();
		int ordinateCoordinate = scanner.nextInt();

		if (abscissaCoordinate > 0 && ordinateCoordinate > 0) {
			System.out.println("The coordinates lies in first quadrant");
		} else if (abscissaCoordinate > 0 && ordinateCoordinate < 0) {
			System.out.println("The coordinates lies in second quadrant");
		} else if (abscissaCoordinate < 0 && ordinateCoordinate < 0) {
			System.out.println("The coordinates lies in third quadrant");
		} else {
			System.out.println("The coordinates lies in fourth quadrant");
		}
	}

}
// TODO: For X and Y to be checked exceptional values for 0