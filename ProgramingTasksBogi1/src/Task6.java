import java.util.Scanner;

public class Task6 {

	public static void main(String[] args) {
		System.out.println("The program will find whether a given year is a leap year or no:");
		Scanner scanner = new Scanner(System.in);

		int givenYear = scanner.nextInt();

		if ((givenYear % 4 == 0) && (givenYear % 100 != 0 || givenYear % 400 == 0)) {
			System.out.println(givenYear + " is leap year!");
		} else {
			System.out.println(givenYear + " is regular year!");
		}
	}

}
// Checked by Bogi