import java.util.Scanner;

public class Task17 {

	public static void main(String[] args) {
		System.out.println(
				"Enter 3 values for the lenths of each side of the triangle and the program will tell you what kind of triangle is it!");

		Scanner scanner = new Scanner(System.in);

		double firstSide = scanner.nextDouble();
		double secondSide = scanner.nextDouble();
		double thirdSide = scanner.nextDouble();

		if (firstSide <= 0 || secondSide <= 0 || thirdSide <= 0) {
			System.out.println("The values must be positive or difrent from 0");
		} else {
			if (firstSide == secondSide && firstSide == thirdSide && secondSide == thirdSide) {
				System.out.println("The triangle is equilateral");
			} else if (firstSide == secondSide || secondSide == thirdSide || firstSide == thirdSide) {
				System.out.println("The triangle is isosceles ");
			} else {
				System.out.println("The triangle is scalene");
			}
		}
	}

}
