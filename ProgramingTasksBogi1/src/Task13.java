import java.util.Scanner;

public class Task13 {

	public static void main(String[] args) {
		System.out.println(
				"The prorgram will take a grade and will return the equivalent description. Grades are E,V,G,A and F");

		Scanner scanner = new Scanner(System.in);

		char gradeSymbol = scanner.nextLine().charAt(0);

		switch (gradeSymbol) {
		case 'E':
			System.out.println("Excellent");
			break;
		case 'V':
			System.out.println("Very Good");
			break;
		case 'G':
			System.out.println("Good");
			break;
		case 'A':
			System.out.println("Average");
			break;
		case 'F':
			System.out.println("Fail");
			break;
		default:
			System.out.println(
					"The symbol that you entered is not from the symbols that are required. Star the program agian!");
			break;

		}
	}

}
