import java.util.Scanner;

public class Task3 {

	public static void main(String[] args) {
		System.out.println("Write a program that allows the user to enter the grade scored in a programming class (0-100).");
		System.out.println("If the user scored a 100 then notify the user that they got a perfect score.");
		System.out.println("Modify the program so that if the user scored a 90-100 it informs the user that they scored an A");
		System.out.println("Modify the program so that it will notify the user of their letter grade");
		System.out.println("0-59 F 60-69 D 70-79 C 80-89 B 90-100 A");
		
		Scanner scanner = new Scanner(System.in);
		
		int gradeValue = scanner.nextInt();
		
		if (gradeValue < 0 || gradeValue > 100){
			System.out.println("The score that you entered is not in the grade scale");
		} else if (gradeValue >= 0 && gradeValue <= 59){
			System.out.println("Sorry, your score is very low your result is F");
		} else if (gradeValue >= 60 && gradeValue <= 69){
			System.out.println("Your result is still low but you'll get D");
		} else if (gradeValue >= 70 && gradeValue <= 79){
			System.out.println("Your result is not that bad you'll get C");
		} else if (gradeValue >= 80 && gradeValue <= 89){
			System.out.println("Your result is very good you'll get B");
		} else if (gradeValue >= 90 && gradeValue < 100){
			System.out.println("Your result is almost perfect you'll get A");
		} else {
			System.out.println("Your result is perfect!");
		}

	}

}
// checked by Bogi 