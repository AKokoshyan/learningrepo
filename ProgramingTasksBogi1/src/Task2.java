import java.util.Scanner;

public class Task2 {

	public static void main(String[] args) {
		System.out.println(
				"Given an int n and the program will return the absolute difference between n and 21, except return double the absolute difference if n is over 21");
		int number;

		Scanner scanner = new Scanner(System.in);
		number = scanner.nextInt();

		int sum = number - 21;

		if (number > 21) {
			System.out.println("The double absolute diffrence is " + 2 * sum);
		} else {
			if (sum < 0) {
				System.out.println("The absolute diffrence is " + (-1) * sum);
			} else {
				System.out.println("The absolute diffrence is " + sum);
			}
		}
	}

}
