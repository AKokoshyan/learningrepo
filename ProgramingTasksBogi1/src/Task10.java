import java.util.Scanner;

public class Task10 {

	public static void main(String[] args) {
		System.out.println("Write a  program to check whether a triangle is Equilateral, Isosceles or Scalene.");
		System.out.println("For this purpuse give value for the angles of the triangle");

		Scanner scanner = new Scanner(System.in);

		int alphaAngle = scanner.nextInt();
		int betaAngle = scanner.nextInt();
		int gamaAngle = scanner.nextInt();

		boolean sumOfAngles = alphaAngle + betaAngle + gamaAngle == 180;

		if (sumOfAngles) {
			if (alphaAngle == betaAngle && betaAngle == gamaAngle) {
				System.out.println("The triangle is equilaterial!");
			} else if (alphaAngle == betaAngle || betaAngle == gamaAngle || alphaAngle == gamaAngle) {
				System.out.println("The triangle is isosceles!");
			} else {
				System.out.println("The triangle is scalene!");
			}
		} else {
			System.out.println(
					"The sum of the values for the angles in degrees are not equal to 180 deg. Please try again! ");
		}
	}

}
