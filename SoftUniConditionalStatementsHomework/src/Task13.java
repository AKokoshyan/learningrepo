import java.util.Scanner;

public class Task13 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		String figure = scan.nextLine().toLowerCase();
		double area = 0;

		switch (figure) {
		case "square":
			double squareSide = Double.parseDouble(scan.nextLine());
			area = squareSide * squareSide;
			break;
		case "rectangle":
			double rectangleSideA = Double.parseDouble(scan.nextLine());
			double rectangleSideB = Double.parseDouble(scan.nextLine());
			area = rectangleSideA * rectangleSideB;
			break;
		case "circle":
			double radius = Double.parseDouble(scan.nextLine());
			area = radius * radius * Math.PI;
			break;
		case "triangle":
			double triangleSide = Double.parseDouble(scan.nextLine());
			double triangleHight = Double.parseDouble(scan.nextLine());
			area = (triangleSide * triangleHight) / 2;
		}

		System.out.printf("%.3f", area);
	}
}