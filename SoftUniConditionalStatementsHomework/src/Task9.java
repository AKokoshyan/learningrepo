import java.util.Scanner;

public class Task9 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		String myPassword = "s3cr3t!P@ssw0rd";
		String userPassword = scan.nextLine();

		if (myPassword.equals(userPassword)) {
			System.out.println("Welcome");
		} else {
			System.out.println("Wrong password!");
		}

	}
}