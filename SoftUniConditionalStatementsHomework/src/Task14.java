import java.util.Scanner;

public class Task14 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int hours = Integer.parseInt(scan.nextLine());
		int minutes = Integer.parseInt(scan.nextLine());

		int timePlusMinutes = (hours * 60) + minutes + 15;

		hours = timePlusMinutes / 60;

		if (hours >= 24) {
			hours = hours % 24;
		}
		minutes = timePlusMinutes % 60;

		System.out.printf("%d:%02d", hours, minutes);
	}
}