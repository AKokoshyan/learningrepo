import java.util.Scanner;

public class StudentTravaling {

	public static void main(String[] args) {
		System.out.println(
				"Enter how much km have the student to travel and during night or day. The program will give the cheapest option!");
		System.out.println("Enter kilometers");
		Scanner scanner = new Scanner(System.in);
		int kilometers = scanner.nextInt();
		System.out.println("Enter day period");
		Scanner scanner1 = new Scanner(System.in);
		String dayPeriod = scanner1.nextLine();
		double taxiStarting = 0.70;
		double taxiDay = 0.79;
		double taxiNight = 0.90;
		double bus = 0.09;
		double train = 0.06;
		String oneOption = "day";
		String secondOption = "night";

		if (kilometers >= 0 && kilometers < 20) {
			if (dayPeriod.equals(oneOption)) {
				System.out.println(taxiStarting + (kilometers * taxiDay));
			} else {
				System.out.println(taxiStarting + (kilometers * taxiNight));
			}

		}
		if (kilometers >= 20 && kilometers < 100) {
			System.out.println(kilometers * bus);
		}
		if (kilometers >= 100) {
			System.out.println(kilometers * train);
		}
	}
}