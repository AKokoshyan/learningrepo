import java.util.Scanner;

public class Task8 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		double length = Double.parseDouble(scan.nextLine());
		String startingMetricUnits = scan.nextLine().toLowerCase();
		String convertToMetricUnits = scan.nextLine().toLowerCase();
		double lengthConverted = 0;

		if (length == 0) {
			lengthConverted = length;
		} else {
			switch (startingMetricUnits) {
			case "m":
				break;
			case "mm":
				length = length / 1000;
				break;
			case "cm":
				length = length / 100;
				break;
			case "mi":
				length = length / 0.000621371192;
				break;
			case "in":
				length = length / 39.3700787;
				break;
			case "km":
				length = length / 0.001;
				break;
			case "ft":
				length = length / 3.2808399;
				break;
			case "yd":
				length = length / 1.0936133;
			}

			switch (convertToMetricUnits) {
			case "m":
				lengthConverted = length;
				break;
			case "mm":
				lengthConverted = length * 1000;
				break;
			case "cm":
				lengthConverted = length * 100;
				break;
			case "mi":
				lengthConverted = length * 0.000621371192;
				break;
			case "in":
				lengthConverted = length * 39.3700787;
				break;
			case "km":
				lengthConverted = length * 0.001;
				break;
			case "ft":
				lengthConverted = length * 3.2808399;
				break;
			case "yd":
				lengthConverted = length * 1.0936133;
			}
		}

		System.out.printf("%f %s", lengthConverted, convertToMetricUnits);
	}
}