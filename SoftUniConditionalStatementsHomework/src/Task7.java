import java.util.Scanner;

public class Task7 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int time1 = Integer.parseInt(scan.nextLine());
		int time2 = Integer.parseInt(scan.nextLine());
		int time3 = Integer.parseInt(scan.nextLine());
		int timeSum = time1 + time2 + time3;

		int minutes = timeSum / 60;
		int seconds = timeSum % 60;

		System.out.printf("%d:%02d", minutes, seconds);

	}
}