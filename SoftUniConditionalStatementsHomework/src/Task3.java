import java.util.Scanner;

public class Task3 {
	public static void main(String[] args) {

		Scanner scan = new Scanner(System.in);

		int num = Integer.parseInt(scan.nextLine());

		boolean isEven = num % 2 == 0;

		if (isEven) {
			System.out.println("even");
		} else {
			System.out.println("odd");
		}
	}
}