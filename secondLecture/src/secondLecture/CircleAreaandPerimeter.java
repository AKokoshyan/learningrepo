package secondLecture;

import java.util.Scanner;

public class CircleAreaandPerimeter {

	public static void main(String[] args) {
		System.out
				.println("The program will compute the area and the parameter of circle. Enter a value for tha radius");
		Scanner scanner = new Scanner(System.in);

		double radiusCircle = scanner.nextDouble();

		double areaOfTheCircle = Math.PI * radiusCircle * radiusCircle;

		double perimeterOfTheCircle = 2 * Math.PI * radiusCircle;

		System.out.println(areaOfTheCircle + " " + perimeterOfTheCircle);
	}

}
