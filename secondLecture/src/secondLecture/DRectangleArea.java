package secondLecture;

import java.util.Scanner;

public class DRectangleArea {

	public static void main(String[] args) {
		System.out.println(
				"The program will compute the rectangle area from 2D coordinates. Enter coordinates (x1,y1) and (x2,y2)");
		Scanner scanner = new Scanner(System.in);
		double firstCoordinateAbsciasa = scanner.nextDouble();
		double firstCoordinatesOrdinate = scanner.nextDouble();
		double secondCoordinatesAbsciasa = scanner.nextDouble();
		double secondCoordinatesOrdinate = scanner.nextDouble();

		double firstSide = firstCoordinateAbsciasa - secondCoordinatesAbsciasa;
		if (firstSide < 0) {
			firstSide = firstSide * (-1);
		}
		double secondSide = firstCoordinatesOrdinate - secondCoordinatesOrdinate;
		if (secondSide < 0) {
			secondSide = secondSide * (-1);
		}

		double rectangleArea = firstSide * secondSide;
		double rectanglePerimeter = 2 * (firstSide + secondSide);

		System.out.println(rectangleArea);
		System.out.println(rectanglePerimeter);
	}

}
