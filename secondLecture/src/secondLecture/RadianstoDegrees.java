package secondLecture;

import java.util.Scanner;

public class RadianstoDegrees {

	public static void main(String[] args) {
		System.out.println("The program will convert the radians to degrees. Enter your value here");

		Scanner scanner = new Scanner(System.in);

		double angleInRadians = scanner.nextDouble();

		int angleInDegrees = (int) Math.toDegrees(angleInRadians);

		System.out.println(angleInDegrees);

	}

}
