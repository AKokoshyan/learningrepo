package secondLecture;

import java.util.Scanner;

public class CelsiustoFahrenheit {

	public static void main(String[] args) {
		System.out.println("The program will convert the temperature from Celsius to Fahrenheit");
		Scanner scanner = new Scanner(System.in);
		double temperatureInCelsius = scanner.nextDouble();

		double temperatureInFahrenheit = ((temperatureInCelsius * 9 / 5.0) + 32);
		System.out.println(temperatureInFahrenheit);
	}

}
