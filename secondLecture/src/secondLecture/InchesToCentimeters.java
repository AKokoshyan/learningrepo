package secondLecture;

import java.util.Scanner;

public class InchesToCentimeters {

	public static void main(String[] args) {
		System.out.println("The program will convert the inches in centimeter");
		System.out.println("Enter the inches here:");

		Scanner scanner = new Scanner(System.in);

		double inches = scanner.nextDouble();

		double inchesToCentimeters = inches * 2.54;

		System.out.println("The inches are equal to " + inchesToCentimeters);

	}

}
