package secondLecture;

import java.util.Scanner;

public class CurrencyConvertor {

	public static void main(String[] args) {
		System.out.println(
				"Enter the curency from that you want to convert and after this the value. You can choose from : BGN, USD, EUR, GBP ");
		Scanner scanner = new Scanner(System.in);
		
		double valueMoney = scanner.nextDouble();
		scanner.nextLine();
		String firstCurrency = scanner.nextLine();
		String secondCurrency = scanner.nextLine();

		if (firstCurrency.equals("BGN")) {
			if (secondCurrency.equals("USD")) {
				System.out.println(valueMoney / 1.79549);
			} else if (secondCurrency.equals("EUR")) {
				System.out.println(valueMoney / 1.95583);
			} else if (secondCurrency.equals("GBP")) {
				System.out.println( valueMoney / 2.53405);
			}
		} else if (firstCurrency.equals("USD")) {
			if (secondCurrency.equals("BGN")) {
				System.out.println(valueMoney * 1.79549);
			} else if (secondCurrency.equals("EUR")) {
				System.out.println((valueMoney * 1.79549) / 1.95583);
			} else if (secondCurrency.equals("GBP")) {
				System.out.println((valueMoney * 1.79549) / 2.53405);
			}
		} else if (firstCurrency.equals("EUR")) {
			if (secondCurrency.equals("BGN")) {
				System.out.println(valueMoney * 1.95583);
			} else if (secondCurrency.equals("USD")) {
				System.out.println((valueMoney * 1.95583) / 1.79549);
			} else if (secondCurrency.equals("GBP")) {
				System.out.println((valueMoney * 1.95583) / 2.53405);
			}
		} else {
			if (secondCurrency.equals("BGN")) {
				System.out.println(valueMoney * 2.53405);
			} else if (secondCurrency.equals("USD")) {
				System.out.println((valueMoney * 2.53405) / 1.79549);
			} else if (secondCurrency.equals("EUR")) {
				System.out.println((valueMoney * 2.53405) / 1.95583);
			}
		}

	}

}
