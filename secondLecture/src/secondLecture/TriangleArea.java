package secondLecture;

import java.util.Scanner;

public class TriangleArea {

	public static void main(String[] args) {
		System.out.println("The program will compute the area of triangle. Give a value for the side and hight");
		Scanner scanner = new Scanner(System.in);
		double sideOfTheTriangle = scanner.nextDouble();
		double hightOfTheTriangle = scanner.nextDouble();

		double areaOfTriangle = (sideOfTheTriangle * hightOfTheTriangle) / 2;

		System.out.println(areaOfTriangle);
	}

}
