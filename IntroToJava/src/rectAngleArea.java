import java.util.Scanner;

public class rectAngleArea {

	public static void main(String[] args) {
		System.out.println("Give two values for a and b, and the program will compute the area of the rectangle!");
		int a , b;
		
		Scanner scanner = new Scanner(System.in);
		a = scanner.nextInt();
		b = scanner.nextInt();
		
		int area = a*b;
		
		System.out.println("The area of the rectangle is: " + area);
	}

}
