import java.util.Scanner;

public class Task28 {
	/**
	 * Return a version of the given array where each zero value in the array is
	 * replaced by the largest odd value to the right of the zero in the array.
	 * If there is no odd value to the right of the zero, leave the zero as a
	 * zero.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. Give a vlue of the elements ");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] == 0 && custemerArray[i + 1] % 2 != 0) {
				custemerArray[i] = custemerArray[i + 1];
			}
		}

		for (int i = 0; i < arrayLenght; i++) {
			System.out.print(custemerArray[i] + " ");
		}
	}

}
