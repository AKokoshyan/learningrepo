import java.util.Scanner;

public class Task10 {
	/**
	 * Write a program to find the second largest element in an array.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. Give a vlue of the elements ");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		int largestNumber;
		int secondLargestNumber;
		
		//TOOD: Check if the array's length is larger than 2
		if (custemerArray[0] > custemerArray[1]) {
			largestNumber = custemerArray[0];
			secondLargestNumber = custemerArray[1];
		} else {
			largestNumber = custemerArray[1];
			secondLargestNumber = custemerArray[0];
		}

		for (int i = 2; i < arrayLenght; i++) {
			if (custemerArray[i] > largestNumber) {
				secondLargestNumber = largestNumber;
				largestNumber = custemerArray[i];
			} else if (custemerArray[i] < largestNumber && custemerArray[i] > secondLargestNumber) {
				secondLargestNumber = custemerArray[i];
			}
		}

		System.out.println("The second largest number is : " + secondLargestNumber);
	}

}
