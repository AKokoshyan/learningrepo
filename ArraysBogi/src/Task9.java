import java.util.Scanner;

public class Task9 {
	/**
	 * Write a program to delete an element at desired position from an array
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for lenght of an array. It must be positive and greater than 0. After this give value of the elements of the array. After this you will be alowed to delete one of the elements!");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You had entered negative number or equal to 0! Enter positive number or greater than 0! Please, try again!!!");
			}
		} while (arrayLenght < 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element : ");
			custemerArray[i] = scanner.nextInt();
		}

		System.out.println("Chose which index to delete");
		//TODO: To validate
		int indexToDelete = scanner.nextInt();

		int secondArrayLenght;

		if (indexToDelete < arrayLenght && indexToDelete > 0) {
			secondArrayLenght = arrayLenght - 1;
		} else {
			secondArrayLenght = arrayLenght;
		}

		int[] arrayWithDeletedElement = new int[secondArrayLenght];
		
		for (int i = 0; i < secondArrayLenght; i++) {
			if (i >= indexToDelete) {
				arrayWithDeletedElement[i] = custemerArray[i + 1];
			} else {
				arrayWithDeletedElement[i] = custemerArray[i];
			}
		}
		
		for (int i = 0; i < secondArrayLenght; i++) {
			System.out.print(arrayWithDeletedElement[i] + " ");
		}
		
	}

}
