import java.util.Scanner;

public class Task29 {
	/**
	 * Given an array of ints, return true if the value 3 appears in the array
	 * exactly 3 times, and no 3's are next to each other.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. Give a vlue of the elements ");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		int repeathingTree = 0;

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] == 3) {
				repeathingTree++;
			}
		}

		boolean isSepareted = true;
		int a = 0;

		for (int i = 1; i < arrayLenght; i++) {
			if (custemerArray[a] == custemerArray[i]) {
				isSepareted = false;
			}
			a++;
		}

		if (repeathingTree == 3 && isSepareted == true) {
			System.out.println("true");
		} else {
			System.out.println("false");
		}
	}

}
