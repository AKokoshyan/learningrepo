import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class Test1 {
	/**
	 * Write a program to count a total number of duplicate elements in an
	 * array.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {

		printArrays();

	}

	public static int validation() {
		Scanner scanner = new Scanner(System.in);
		int customerNumber;
		do {

			customerNumber = scanner.nextInt();
			if (customerNumber <= 0) {
				System.out.println("The number that you entered is negative! Try again");
			}
		} while (customerNumber <= 0);
		return customerNumber;
	}

	public static int[] array(int inputArray) {
		Scanner scanner = new Scanner(System.in);
		int[] array = new int[inputArray];
		for (int i = 0; i < inputArray; i++) {
			array[i] = scanner.nextInt();
		}
		return array;

	}

	public static void printArrays() {
		int lengthArrays = validation();
		int[] array1 = array(lengthArrays);

		System.out.println("First arrays elements are : ");
		for (int i = 0; i < lengthArrays; i++) {
			System.out.print(array1[i] + " ");
		}
		int[] array2 = array(lengthArrays);
		System.out.println();
		System.out.println("Second arrays elements are : ");
		for (int i = 0; i < lengthArrays; i++) {
			System.out.print(array2[i] + " ");
		}
	}
}
