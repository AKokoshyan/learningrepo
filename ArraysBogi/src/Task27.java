import java.util.Scanner;

public class Task27 {
	/**
	 * Given an array length 1 or more of ints, return the difference between
	 * the largest and smallest values in the array. Note: the built-in
	 * Math.min(v1, v2) and Math.max(v1, v2) methods return the smaller or
	 * larger of two values.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. Give a vlue of the elements and the program will compute the diffrence between the largest and the smallest element");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		int largestNumber = custemerArray[0];

		for (int i = 0; i < arrayLenght; i++) {
			if (largestNumber < custemerArray[i]) {
				largestNumber = custemerArray[i];
			}
		}

		int smallestNumber = custemerArray[0];

		for (int i = 0; i < arrayLenght; i++) {
			if (smallestNumber > custemerArray[i]) {
				smallestNumber = custemerArray[i];
			}
		}

		int diffrenceBetweenElements = largestNumber - smallestNumber;

		System.out.println("The diffrence between " + largestNumber + " and " + smallestNumber + " is : "
				+ diffrenceBetweenElements);
	}

}
