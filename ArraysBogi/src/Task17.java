import java.util.Scanner;

public class Task17 {
	/**
	 * Write a program to put even and odd elements of array in two separate
	 * array.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Enter a number to declare lenght of an array! They must be positive and greater than 0! Program will find the even and odd elements and it will put them in two separate arrays!");

		int arrayLenght;

		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"The number that you entered for lenght of tha array is negative or equal to 0. Try again with positive number and greater than 0");
			}

		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element : ");
			custemerArray[i] = scanner.nextInt();
		}

		int[] evenArray = new int[arrayLenght];
		int[] oddArray = new int[arrayLenght];
		int evenArrayLenght = 0;
		int oddArrayLenght = 0;

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] % 2 == 0) {
				evenArray[evenArrayLenght] = custemerArray[i];
				evenArrayLenght++;
			} else {
				oddArray[oddArrayLenght] = custemerArray[i];
				oddArrayLenght++;
			}
		}

		for (int i = 0; i < evenArrayLenght; i++) {
			System.out.print(evenArray[i] + " ");
		}
		System.out.println();

		for (int i = 0; i < oddArrayLenght; i++) {
			System.out.print(oddArray[i] + " ");
		}
	}

}
