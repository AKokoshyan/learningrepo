import java.util.Scanner;

public class Task18 {
	/**
	 * Write a program to search an element in an array
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Enter a number to declare lenght of an array! They must be positive and greater than 0! Program will search an element in an array!");

		int arrayLenght;

		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"The number that you entered for lenght of tha array is negative or equal to 0. Try again with positive number and greater than 0");
			}

		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element ");
			custemerArray[i] = scanner.nextInt();
		}

		System.out.println(
				"Enter a number to check is it in the array and if is it the program will tell you yes or no and it's position in the array!");
		int custemerSearch = scanner.nextInt();

		boolean isContaining = false;
		int arrayIndex = 0;

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] == custemerSearch) {
				isContaining = true;
				arrayIndex = i;
				break;
			}
		}
		if (isContaining) {
			System.out.println("Yes, " + custemerSearch + " is containing in the array on position ! " + arrayIndex);
		} else {
			System.out.println("No, " + custemerSearch + " is not containing in the array ! ");
		}

	}
}
