import java.util.Scanner;

public class Task12 {
	/**
	 * Write a program to print all negative elements in an array.s
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Enter a number wiche will be the lenght of an array, and after this give a value to every element of the array. Program will show you all negative numbers");
		System.out.println(
				"Note!!! The number wiche will intialize the lenght of the array must be positive or greater than 0");
		int arrayLenght;
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"The number that you entered for lenght of tha array is negative or equal to 0. Try again with positive number and greater than 0");
			}

		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		System.out.print("The negative numbers from the elements are : ");

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] < 0) {
				System.out.print(custemerArray[i] + " ");
			}
		}
	}

}
