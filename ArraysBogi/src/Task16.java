import java.util.Scanner;

public class Task16 {
	/**
	 * Write a program to merge two array to third array
	 * 
	 * @param args
	 */

	public static int getValidArrayLength() {
		Scanner scanner = new Scanner(System.in);
		int arrayLength;
		do {
			arrayLength = scanner.nextInt();
			if (arrayLength <= 0) {
				System.out.println(
						"The number that you entered for lenght of tha array is negative or equal to 0. Try again with positive number and greater than 0");
			}

		} while (arrayLength <= 0);

		return arrayLength;
	}

	public static void initializeArray(int[] array) {
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < array.length; i++) {
			System.out.println("Enter [" + i + "] element of the first array : ");
			array[i] = scanner.nextInt();
		}
		
	}

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Enter two numbers to declare lenght of two arrays! They must be positive and greater than 0! Program will merge the two arrays in one!");

		int firstArrayLength = getValidArrayLength();
		System.out.println("Enter second array length");
		int secondArrayLength = getValidArrayLength();

		int[] firstCustemerArray = new int[firstArrayLength];
		int[] secondCustemerArray = new int[secondArrayLength];

		initializeArray(firstCustemerArray);
		initializeArray(secondCustemerArray);

		int mergeArrayLength = firstArrayLength + secondArrayLength;
		int[] mergeArray = new int[mergeArrayLength];

		for (int i = 0; i < firstArrayLength; i++) {
			mergeArray[i] = firstCustemerArray[i];
		}

		for (int i = 0; i < secondArrayLength; i++) {
			mergeArray[firstArrayLength + i] = secondCustemerArray[i];
		}

		for (int i = 0; i < mergeArrayLength; i++) {
			System.out.print(mergeArray[i] + " ");
		}
	}

}
