import java.util.Scanner;

public class Task15 {
	/**
	 * Write a program to count total number of even and odd elements in an
	 * array
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number to declare lenght of an array! It must be positive and greater than 0! Program will compute how much from the elements are even and odd!");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"The number that you entered for lenght of tha array is negative or equal to 0. Try again with positive number and greater than 0");
			}

		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element : ");
			custemerArray[i] = scanner.nextInt();
		}

		int evenElements = 0;

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] % 2 == 0) {
				evenElements++;
			}
		}

		int oddElements = arrayLenght - evenElements;
		System.out
				.println("In the array are " + evenElements + " even elements and " + oddElements + " odd elements !");
	}

}
