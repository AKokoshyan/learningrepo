import java.util.Scanner;

public class Task3 {
	/**
	 * Write a program to count a total number of duplicate elements in an
	 * array. Test Data : Input the number of elements to be stored in the array
	 * 3 Input 3 elements in the array : element - 0 : 5 element - 1 : 1 element
	 * - 2 : 1 Expected Output : Total number of duplicate elements found in the
	 * array is : 1
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the array length ");
		int arrayLenth = scanner.nextInt();
		int[] array = new int[arrayLenth];
		int[] repeatingElements = new int[arrayLenth];
		int repeatingElementsSize = 0;

		// Initialize array
		for (int i = 0; i < arrayLenth; i++) {
			System.out.println("Enter element [" + i + "] ");
			array[i] = scanner.nextInt();
		}

		boolean isElementRepeating;
		for (int i = 0; i < arrayLenth; i++) {
			// First we check if array[i] was found as a repeating element
			// during some previous iteration
			isElementRepeating = false;
			for (int j = 0; j < repeatingElementsSize; j++) {
				if (array[i] == repeatingElements[j]) {
					// If we get here, that means array[i] has been already
					// registered as a repeating element.'
					isElementRepeating = true;
					break;
				}
			}

			if (isElementRepeating) {
				continue;
			}

			// If we get here, that means the current element array[i] was not
			// previously found as a repeating one and we need to check if it is
			// repeating in the rest of the elements of the array
			for (int j = i + 1; j < arrayLenth; j++) {
				if (array[i] == array[j]) {
					// If we get here that means array[i] is a repeating element
					// and we need to register it as one
					repeatingElements[repeatingElementsSize] = array[i];
					repeatingElementsSize++;
					break;
				}
			}
		}

		System.out.println("The repeating elements are:");
		for (int i = 0; i < repeatingElementsSize; i++) {
			System.out.println(repeatingElements[i]);
		}
	}
}