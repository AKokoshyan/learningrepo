import java.util.Scanner;

public class Task11 {
	/**
	 * Write a program to find the second smallest element in an array.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. Give a vlue of the elements ");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		int smallestNumber;
		int secondSmallestNumber;

		//TODO: check the array's length
		if (custemerArray[0] < custemerArray[1]) {
			smallestNumber = custemerArray[0];
			secondSmallestNumber = custemerArray[1];
		} else {
			smallestNumber = custemerArray[1];
			secondSmallestNumber = custemerArray[0];
		}

		for (int i = 2; i < arrayLenght; i++) {
			if (custemerArray[i] < smallestNumber) {
				secondSmallestNumber = smallestNumber;
				smallestNumber = custemerArray[i];
			} else if (custemerArray[i] < smallestNumber && custemerArray[i] > secondSmallestNumber) {
				secondSmallestNumber = custemerArray[i];
			}
		}
		System.out.println("The second smallest number is : " + secondSmallestNumber);
	}

}
