import java.util.Scanner;

public class Task1 {
	/**
	 * Write a program to find the sum of all elements of the array. Test Data :
	 * Input the number of elements to be stored in the array 3 Input 3 elements
	 * in the array : element - 0 : 2 element - 1 : 5 element - 2 : 8 Expected
	 * Output : Sum of all elements stored in the array is : 15
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Enter a number which will inicialize the lenth of an array, the number must be positive and greater than 0!");
		int lenthOfArray;
		do {
			lenthOfArray = scanner.nextInt();
			if (lenthOfArray <= 0) {
				System.out.println("The number that you entered is negative or equal to 0. Please, try again!");
			}
		} while (lenthOfArray <= 0);

		int[] array = new int[lenthOfArray];

		for (int i = 0; i < lenthOfArray; i++) {
			System.out.println("Enter " + (i + 1) + " element of the array! :");
			array[i] = scanner.nextInt();
		}

		int sumOfArraysElements = 0;
		for (int i = 0; i < lenthOfArray; i++) {
			sumOfArraysElements += array[i];
		}

		System.out.println("Sum of all elements stored in the array is : " + sumOfArraysElements);
	}

}
