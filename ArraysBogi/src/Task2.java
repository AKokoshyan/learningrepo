import java.util.Scanner;

public class Task2 {
	/**
	 * Write a program to COPY the elements one array into another array.
	 * Test Data : Input the number of
	 * elements to be stored in the array 3 Input 3 elements in the array :
	 * element - 0 : 15 element - 1 : 10 element - 2 : 12 Expected Output : The
	 * elements stored in the first array are : 15 10 12 The elements copied
	 * into the second array are : 15 10 12
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(" Write a program to COPY the elements one array into another array.");

		int arrayLenth;

		do {
			System.out.println("Enter a number for arrays lenth. It must be positive and greater than 0 ");
			arrayLenth = scanner.nextInt();
			if (arrayLenth <= 0) {
				System.out.println("The number that you entered is negative or equal to 0");
			}

		} while (arrayLenth <= 0);

		int[] firstArray = new int[arrayLenth];
		int[] secondArrray = new int[arrayLenth];

		for (int i = 0; i < arrayLenth; i++) {
			System.out.println("Enter " + (i + 1) + " element of the array! : ");
			firstArray[i] = scanner.nextInt();
		}

		System.out.println("The elements stored in first array are : ");

		for (int i = 0; i < arrayLenth; i++) {
			System.out.print(firstArray[i] + " ");
		}

		for (int i = 0; i < arrayLenth; i++) {
			secondArrray[i] = firstArray[i];

		}

		System.out.println("\nThe elements stored in second array are : ");

		for (int i = 0; i < arrayLenth; i++) {
			System.out.print(secondArrray[i] + " ");
		}
	}

}
