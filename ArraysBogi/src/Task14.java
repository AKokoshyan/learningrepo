import java.util.Scanner;

public class Task14 {
	/**
	 * Write a program to find maximum and minimum element in an array.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number to declare lenght of an array! It must be positive and greater than 0! Program will print the maximum and minimum element of the array!");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"The number that you entered for lenght of tha array is negative or equal to 0. Try again with positive number and greater than 0");
			}

		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		int maxNumberOfArraysElement = custemerArray[0];
		int minNumberOfArraysElement = custemerArray[0];

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] > maxNumberOfArraysElement) {
				maxNumberOfArraysElement = custemerArray[i];
			}
			if (custemerArray[i] < minNumberOfArraysElement) {
				minNumberOfArraysElement = custemerArray[i];
			}
		}

		System.out.println("The greatest value of the elements of tha array is : " + maxNumberOfArraysElement);
		System.out.println("The smallest value of the elements of the array is : " + minNumberOfArraysElement);

	}

}
