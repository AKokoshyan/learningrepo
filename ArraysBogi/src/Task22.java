import java.util.Scanner;

public class Task22 {
	/**
	 * Given an array of ints, return true if the array contains no 1's and no
	 * 3's.
	 *  lucky13([0, 2, 4]) - true 
	 *  lucky13([1, 2, 3]) - false 
	 *  lucky13([1, 2, 4]) - false
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. After this give value of the elements if you put 1 or 3 as a value of the elements the program will print false");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}
		boolean isContainingOnesOrThrees = false;

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] == 1 || custemerArray[i] == 3) {
				isContainingOnesOrThrees = true;
			}
		}
		if (isContainingOnesOrThrees) {
			System.out.println("false");
		} else {
			System.out.println("true");
		}
	}

}
