import java.util.Scanner;

import javax.swing.plaf.synth.SynthSeparatorUI;

public class Task24 {
	/**
	 * Return an array that is "left shifted" by one -- so {6, 2, 5, 3} returns
	 * {2, 5, 3, 6}. You may modify and return the given array, or return a new
	 * array.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. After this give value of the elements the program will print the array in reverse");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];
		int[] outputArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element : ");
			custemerArray[i] = scanner.nextInt();
		}
		
	}

}
