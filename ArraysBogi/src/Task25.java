import java.util.Scanner;

public class Task25 {
	/**
	 * Given a non-empty array of ints, return a new array containing the
	 * elements from the original array that come after the last 4 in the
	 * original array. The original array will contain at least one 4. Note that
	 * it is valid in java to create an array of length 0
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int arrayLenght;
		System.out.println(
				"Enter a number for a lenght of an array the number must be positive and greater than 0. After this give value of the elements at least one of the elements must be equal to 4 and program will print the elements from the last element containing 4 of the first array in  another array");
		do {
			arrayLenght = scanner.nextInt();
			if (arrayLenght <= 0) {
				System.out.println(
						"You have enetered negative number or equal to 0. Enter positive number and greater than 0. Try again!");
			}
		} while (arrayLenght <= 0);

		int[] custemerArray = new int[arrayLenght];
		int[] outputArray = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			custemerArray[i] = scanner.nextInt();
		}

		int lastFour = 0;
		int diferenceInLenght = 0;

		for (int i = 0; i < arrayLenght; i++) {
			if (custemerArray[i] == 4) {
				lastFour = i + 1;
				diferenceInLenght = i + 1;
			}
		}

		int outputArrayLenght = arrayLenght - diferenceInLenght;

		for (int i = 0; i < outputArrayLenght; i++) {
			outputArray[i] = custemerArray[lastFour];
			lastFour++;
		}

		for (int i = 0; i < outputArrayLenght; i++) {
			System.out.print(outputArray[i] + " ");
		}
	}

}
