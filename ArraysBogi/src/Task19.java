import java.util.Scanner;

public class Task19 {

	public static void main(String[] args) {

		System.out.println("Enter 10 integer numbers of an array and the program will then display :");
		System.out.println("The array is growing, if the numbers of the row are increasing");
		System.out.println("The array is decreasing, if the numbers of the row are decreasing");
		System.out.println("The array is constant, if all numbers of the array are equal");
		System.out.println("The array is growing and decreasing, if the nubers are mixed ");

		Scanner scanner = new Scanner(System.in);
		int arrayLenght = scanner.nextInt();
		int[] array = new int[arrayLenght];

		for (int i = 0; i < arrayLenght; i++) {
			System.out.println("Enter [" + i + "] element :");
			array[i] = scanner.nextInt();
		}

	}

}
