import java.util.Scanner;

public class Task2 {
	/**
	 * Write a C program to find diameter, circumference and area of circle
	 * using functions.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		double customerRadios = scanner.nextDouble();

		printAll(customerRadios);

	}

	public static double getDiameter(double customerRadios) {
		double diameter = 2 * customerRadios;
		return diameter;
	}

	public static double getCircumference(double customerRadios) {
		double cirumference = Math.PI * getDiameter(customerRadios);
		return cirumference;
	}

	public static double getArea(double customerRadius) {
		double area = Math.PI * Math.pow(customerRadius, 2);
		return area;
	}

	public static void printAll(double customerNumber) {
		System.out.println(
				"The diameter of the circle with radius " + customerNumber + " is " + getDiameter(customerNumber));
		System.out.println("The circumference of the circle with radius " + customerNumber + " is "
				+ getCircumference(customerNumber));
		System.out.println("The area of the circle wiht radius " + customerNumber + " is " + getArea(customerNumber));
	}

}
