import java.util.Scanner;

public class Task7 {
	/**
	 * 
	 * Write a C program to print all strong numbers between given interval
	 * using functions
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println(
				"Enter a number and the program will find all strong number of the row from 1 to your number!");
		int customerNumber = scanner.nextInt();

		printAllStrongNumber(customerNumber);

	}

	public static void printAllStrongNumber(int input) {
		System.out.println("The strong numbers are : ");
		for (int i = 1; i < input; i++) {
			if (isStrong(i)) {
				System.out.print(i + " ");
			}
		}
	}

	public static boolean isStrong(int input) {
		int repeater = input;
		int currentDigit;
		int currentFactorial = 0;

		while (repeater > 0) {
			currentDigit = repeater % 10;
			currentFactorial += getFactorial(currentDigit);
			repeater /= 10;
		}
		if (currentFactorial == input) {
			return true;
		}
		return false;
	}

	public static int getFactorial(int input) {
		int factorial = 1;
		for (int i = 1; i < input; i++) {
			factorial *= i;
		}
		return factorial;
	}

}
