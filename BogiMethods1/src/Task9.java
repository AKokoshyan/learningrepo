import java.util.Scanner;

public class Task9 {
	/**
	 * Write a C program to find all perfect numbers between given interval
	 * using functions
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a number and the program will compute whiche numbers of the row are perfect");

		int customerNumber = scanner.nextInt();

		printAllPerfectNums(customerNumber);

	}

	public static void printAllPerfectNums(int customerNumber) {
		System.out.println("The perfect numbers of the row are : ");
		for (int i = 2; i < customerNumber; i++) {
			if (isPerfect(i)) {
				System.out.print(i + " ");
			}
		}
	}

	public static boolean isPerfect(int input) {
		boolean isPerfect = false;
		int sum = 1;
		for (int i = 2; i < input; i++) {
			if (input % i == 0) {
				sum += i;
			}
		}
		if (input == sum) {
			isPerfect = true;
		}
		return isPerfect;
	}
}
