import java.util.Scanner;

public class Task4 {
	/**
	 * Write a C program to check whether a number is even or odd using
	 * functions.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a number and the program will tell you is even or odd");

		int customerNumber = scanner.nextInt();

		printEvenOrOdd(customerNumber);
	}

	// The method below is printing either number is even or is odd
	public static void printEvenOrOdd(int customerNumber) {

		if (isEven(customerNumber)) {
			System.out.println("The number is even");
		} else {
			System.out.println("The number is odd");
		}
	}

	// The method below is checking is the number even or odd and return boolean
	// value!
	public static boolean isEven(int customerNumber) {
		if (customerNumber % 2 == 0) {
			return true;
		}

		return false;
	}
}
