import java.util.Scanner;

public class Task1 {
	/**
	 * Write a C program to find cube of any number using function.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a number and the program will find the cube of the number!");

		int customerNumber = scanner.nextInt();

		int customerNumberCube = getCube(customerNumber);

		System.out.println("The cube of " + customerNumber + " is : " + customerNumberCube);
	}

	// The method below compute the power cube of the customer number.
	public static int getCube(int custemerNumber) {
		int customerNumberCube = custemerNumber * custemerNumber * custemerNumber;
		return customerNumberCube;
	}
}
