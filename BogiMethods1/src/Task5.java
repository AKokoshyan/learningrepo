import java.util.Scanner;

public class Task5 {
	/**
	 * Write a C program to check whether a number is prime, Armstrong or
	 * perfect number using functions.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a number and the program will check is it Armstrong, Prime or Perfect");

		int customerNumber = scanner.nextInt();

		printAllChecks(customerNumber);
	}

	public static void printAllChecks(int input) {
		if (isArmstrong(input)) {
			System.out.println("The number is Armstrong number!");
		} else {
			System.out.println("The number is not Armstrong number!");
		}
		if (isPrime(input)) {
			System.out.println("The number is Prime!");
		} else {
			System.out.println("The number is not Prime!");
		}
		if (isPerfect(input)) {
			System.out.println("The number is Perfect!");
		} else {
			System.out.println("The number is not Perfect!");
		}
	}

	public static boolean isArmstrong(int input) {
		int digitCounter = 0;
		int repeater = input;
		while (repeater > 0) {
			repeater /= 10;
			digitCounter++;
		}
		int sum = 0;
		int currentDigit;
		int secondRepeater = input;

		for (int i = 0; i < digitCounter; i++) {
			currentDigit = secondRepeater % 10;
			sum += Math.pow(currentDigit, digitCounter);
			secondRepeater /= 10;
		}
		if (input == sum) {
			return true;
		} else {
			return false;
		}
	}

	public static boolean isPrime(int input) {
		boolean isPrime = true;
		for (int i = 2; i < input; i++) {
			if (input % i == 0) {
				isPrime = false;
			}
		}
		return isPrime;
	}

	public static boolean isPerfect(int input) {
		boolean isPerfect = false;
		int sum = 1;
		for (int i = 2; i < input; i++) {
			if (input % i == 0) {
				sum += i;
			}
		}
		if (input == sum) {
			isPerfect = true;
		}
		
		return isPerfect;
	}
}
