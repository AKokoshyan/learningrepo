import java.util.Scanner;

public class Task8 {
	/**
	 * Write a C program to print all Armstrong numbers between given interval
	 * using functions.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a number and the program will compute all Armstrong number of 1 to your number!");
		int lowerNumber = scanner.nextInt();
		int upperNumber = scanner.nextInt();

		printAllArmstrongNumber(lowerNumber, upperNumber);

	}

	public static void printAllArmstrongNumber(int lower, int upper) {
		System.out.println("All Armstrong numbers of the row are : ");
		for (int i = lower; i < upper; i++) {
			if (isArmstrong(i)) {
				System.out.print(i + " ");
			}
		}
	}

	public static boolean isArmstrong(int input) {
		int digitCounter = 0;
		int repeater = input;
		while (repeater > 0) {
			repeater /= 10;
			digitCounter++;
		}
		int sum = 0;
		int currentDigit;
		int secondRepeater = input;

		for (int i = 0; i < digitCounter; i++) {
			currentDigit = secondRepeater % 10;
			sum += Math.pow(currentDigit, digitCounter);
			secondRepeater /= 10;
		}
		if (input == sum) {
			return true;
		} else {
			return false;
		}
	}

}
