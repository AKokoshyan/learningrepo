import java.util.Scanner;

public class Test1 {
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int customerNumber = scanner.nextInt();

		if (isPerfect(customerNumber)) {
			System.out.println("The number is perfect");
		} else {
			System.out.println("The number is not perfect");
		}
	}

	public static boolean isPerfect(int customerNumber) {
		int currentNumber;
		int currentFactorial = 1;
		int sumOfFactorials = 0;
		int customerNumberRepeater = customerNumber;

		while (customerNumberRepeater > 0) {
			currentNumber = customerNumberRepeater % 10;
			for (int i = 1; i <= currentNumber; i++) {
				currentFactorial *= i;
			}
			sumOfFactorials += currentFactorial;
			currentFactorial = 1;
			customerNumberRepeater /= 10;
		}

		if (customerNumber == sumOfFactorials) {
			return true;
		}
		return false;
	}
}
