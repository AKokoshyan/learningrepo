import java.util.Scanner;

public class Task3 {
	/**
	 * Write a C program to find maximum and minimum between two numbers using
	 * functions
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter two numbers and program will tell you which is the biggest and the smallest number ");

		int customerFirstNumber = scanner.nextInt();
		int customerSecondNumber = scanner.nextInt();

		printBiggestSmallestNumber(customerFirstNumber, customerSecondNumber);
	}

	// The method below is printing the biggest and smallest number!
	public static void printBiggestSmallestNumber(int firstNumber, int smallestNumber) {
		System.out.println("The bigest number is : " + max(firstNumber, smallestNumber));
		System.out.println("The smallest number is : " + min(firstNumber, smallestNumber));
	}

	// The method below is comparing two numbers from the console and give the
	// biggest number!
	public static int max(int firstNumber, int secondNumber) {
		if (firstNumber > secondNumber) {
			return firstNumber;
		} else {
			return secondNumber;
		}
	}

	// The method below is comparing two numbers from the console and give the
	// smallest number!
	public static int min(int firstNumber, int secondNumber) {
		if (firstNumber < secondNumber) {
			return firstNumber;
		} else {
			return secondNumber;
		}
	}

}
