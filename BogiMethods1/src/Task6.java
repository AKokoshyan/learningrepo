import java.util.Scanner;

public class Task6 {
	/**
	 * Write a C program to find all prime numbers between given interval using
	 * functions.
	 * 
	 * @param args
	 */

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter a number and the program will find all prime numbers from 1 to your number!");
		int customerNumber = scanner.nextInt();

		primeNumbers(customerNumber);

	}

	// The method below is printing all prime numbers of the row.
	public static void primeNumbers(int input) {
		System.out.println("Prime numbers of the row are : ");
		for (int i = 1; i < input; i++) {
			if (isPrime(i)) {
				System.out.print(i + " ");
			}
		}
	}

	// The method below is checking the number is prime or not.
	public static boolean isPrime(int input) {
		boolean isPrime = true;
		for (int i = 2; i < input; i++) {
			if (input % i == 0) {
				isPrime = false;
			}
		}
		return isPrime;
	}

}
