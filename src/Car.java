/**
 * Created by Tonio on 4/23/2017.
 */
public class Car {
    private int maxSpeed;
    private double currentSpeed;
    private String color;
    private int currentGear;
    private Person owner;
    private double price;
    private boolean isSportCar;
    private String model;

    // Constructors
    public Car() {
        this.maxSpeed = 190;
    }

    public Car(String model, boolean isSportCar, String color) {
        this.currentSpeed = 50.0;
        this.currentGear = 3;
        this.model = model;
        this.isSportCar = isSportCar;
        this.color = color;
    }

    public Car(String model, boolean isSportCar, String color, double price, int maxSpeed) {
        this(model, isSportCar, color);
        this.price = price;
        if (isSportCar = true && maxSpeed >= 200) {
            this.maxSpeed = maxSpeed;
        } else {
            this.maxSpeed = maxSpeed;
        }
    }

    public Car(double currentSpeed, String color, int currentGear, Person owner) {
        this();
        this.currentSpeed = currentSpeed;
        this.color = color;
        this.currentGear = currentGear;
        this.owner = owner;
    }

    //setters and getters
    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public int getCurrentGear() {
        return this.currentGear;
    }

    public void setCurrentSpeed(double currentSpeed) {
        if (currentSpeed >= 0.0 && currentSpeed <= maxSpeed) {
            this.currentSpeed = currentSpeed;
        }
    }

    public double getCurrentSpeed() {
        return this.currentSpeed;
    }

    public Person getOwner() {
        return this.owner;
    }

    public double getPrice() {
        return this.price;
    }

    public void setColor(String newColor) {
        this.color = newColor;
    }

    public void setGear(int currentGear) {
        if (currentGear >= 1 && currentGear <= 7) {
            this.currentGear = currentGear;
        }
    }

    // Public methods
    public void accelerate() {
        if (currentSpeed <= maxSpeed - 10) {
            this.currentSpeed += 10;
        }
    }

    public void changeGearUp() {
        if (currentGear >= 1 && currentGear < 7) {
            ++currentGear;
        }
    }

    public void changeGearDown() {
        if (currentGear >= 2 && currentGear <= 7) {
            --currentGear;
        }
    }


    public int comparePrices(Car other) {
        if (this.price > other.price) {
            return 1;
        } else if (this.price == other.price) {
            return 0;
        }
        return -1;
    }

    public double calculateCarPriceForScrap(double metalPrice) {
        double coeficent = 0.2;
        double price = 0;
        if (this.color.equals("black") || this.color.equals("white") || this.isSportCar == true) {
            coeficent += 0.05;
        }

        price = metalPrice * coeficent;
        return price;
    }

    // private methods
}

