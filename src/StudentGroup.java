/**
 * Created by Tonio on 4/20/2017.
 */
public class StudentGroup {
    private String groupSubject;
    private Student[] students;
    private int freePlaces;
    private int counter;

    public StudentGroup() {
        this.counter = 0;
        this.students = new Student[5];
        this.freePlaces = 5;
    }

    public StudentGroup(String subject) {
        this();
        this.groupSubject = subject;
    }

    public void addStudent(Student newStudent) {
        if (freePlaces > 0 && this.groupSubject.equals(newStudent.getSubject())) {
            freePlaces--;
            students[counter] = newStudent;
            counter++;
        }
    }

    public void emptyGroup() {
        this.freePlaces = 5;
        this.counter = 0;
        for (int i = 0; i < students.length; i++) {
            students[i] = null;
        }
    }

    public String bestStudent() {
        String bestStudentName = null;
        for (int i = 0; i < students.length; i++) {
            for (int k = 1; k < students.length; k++) {
                if (students[i].getGrade() < students[k].getGrade()) {
                    bestStudentName = students[k].getName();
                }
            }

        }
        return bestStudentName;
    }

    public void printStudentGroup() {
        for (int i = 0; i < students.length; i++) {
            System.out.println("The name of the " + i + " student is : " + students[i].getName());
            System.out.println("The grade of the " + i + " student is : " + students[i].getGrade());
        }
    }


}
