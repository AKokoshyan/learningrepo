import java.util.Random;
import java.util.Scanner;

/**
 * Created by Tonio on 5/5/2017.
 */
public class MainClass {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int arrayLength = scanner.nextInt();
        int[] array = new int[arrayLength];

        Random element = new Random();

        for (int i = 0; i < arrayLength; i++) {
            array[i] = element.nextInt(15);
        }
        for (int i = 0; i < arrayLength; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();

        array = Sorting.ascendingBubbleSort(array);
        for (int i = 0; i < arrayLength; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        System.out.println(ArrayDuplicateElement.getNumberDuplicateElem(array));
    }
}
