package chapter10;

/**
 * Created by Tonio on 4/25/2017.
 */
public class Sequence {
    private static int currentValue = -1;

    // Intentionally deny instantiation of this class
    private Sequence() {
    }

    // Static method
    public static int nextValue() {
        currentValue++;
        return currentValue;
    }
}

