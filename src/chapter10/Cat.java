package chapter10;

/**
 * Created by Tonio on 4/25/2017.
 */
public class Cat {
    // Field name
    private String name;
    // Field color
    private String color;
    //Number of cats
    public static int numberOfCats = 0;

    // Default constructor
    public Cat() {
        ++numberOfCats;
        this.name = "Unnamed"+numberOfCats;
        this.color = "gray";
    }

    // Constructor with parameters
    public Cat(String name, String color) {
        ++numberOfCats;
        this.name = name+numberOfCats;
        this.color = color;
    }

    // Getter of property name
    public String getName() {
        return this.name;
    }

    // Setter of property name
    public void setName(String name) {
        this.name = name;
    }

    // Getter of property color
    public String getColor() {
        return this.color;
    }

    // Setter of property color
    public void setColor(String color) {
        this.color = color;
    }

    // Method sayMiau
    public void sayMiau() {
        System.out.printf("Cat %s said: Miauuuuuu!%n", name);
    }
}

