import java.util.Scanner;

/**
 * Created by Tonio on 5/31/2017.
 */
public class Test {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int num = Integer.parseInt(scanner.nextLine());
        int base1 = Integer.parseInt(scanner.nextLine());

        String result = repeatString(num, base1);

        System.out.println(result);
    }


    public static String repeatString(int num, int base1) {
        String result = "";
        int remainder = 0;


        while (num > 0) {

            remainder = num % base1;
            result = remainder + result;
            num /= num;
        }

        return result;

    }
}
