import java.util.Random;

/**
 * Created by Tonio on 4/26/2017.
 */
public class SortingDemo {
    public static void main(String[] args) {
        Random randomNumber = new Random();
        int[] array = new int[5];

        //Initialize the elements of the array with "Random" class method!
        for (int i = 0; i < array.length; i++) {
            array[i] = randomNumber.nextInt(15);
        }

        //Printing the elements of the array to check which are the elements of the array!
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        System.out.println(isSorted(array));

        //Call the sorting method to sort the elements of the array!
        array = Sorting.ascendingBubbleSort(array);
        //array = Sorting.descendingBubbleSort(array);

        //Printing the elements of the array to check is the sorting method working properly!
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
        System.out.println();
        System.out.println(isSorted(array));
        System.out.println(sumArrayRecursion(array, 4));
    }

    public static int sumArrayRecursion(int[] array, int i) {
        if (i == 0) {
            return array[i];
        } else {

            return array[i] + sumArrayRecursion(array, i - 1);
        }

    }

    //The method below is checking is the array sorted!
    public static boolean isAscendigSorted(int[] array) {
        boolean isSorted = true;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] > array[i + 1]) {
                isSorted = false;
            }
        }
        return isSorted;
    }

    public static boolean isDescendingSorted(int[] array) {
        boolean isSorted = true;
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] < array[i + 1]) {
                isSorted = false;
            }
        }
        return isSorted;
    }

    public static boolean isSorted(int[] array) {
        boolean isSorted = false;
        if (isAscendigSorted(array)) {
            isSorted = true;
        } else if (isDescendingSorted(array))
            isSorted = true;
        return isSorted;
    }
}
