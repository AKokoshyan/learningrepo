/**
 * Created by Tonio on 4/18/2017.
 */
public class Computer {

    private int year;
    private double price;
    private boolean isNotebook;
    private double hardDiskMemory;
    private double freeMemory;
    private String operationSystem;

    public Computer(int year, double price, boolean isNotebook, int hardDiskMemory, double freeMemory, String operationSystem) {
        this.year = year;
        this.price = price;
        this.isNotebook = isNotebook;
        this.hardDiskMemory = hardDiskMemory;
        this.freeMemory = freeMemory;
        this.operationSystem = operationSystem;
    }

    public Computer() {
        this.isNotebook = false;
        this.operationSystem = "Win XP";
    }

    public Computer(int year, double price, double hardDiskMemory, double freeMemory) {
        this();
        this.year = year;
        this.price = price;
        this.hardDiskMemory = hardDiskMemory;
        this.freeMemory = freeMemory;
    }

    public void setYear(int year) {
        int defaultYear = 1980;
        if (year < 1980 || year > 2017) {
            this.year = defaultYear;
        } else {
            this.year = year;
        }
    }

    public int getYear() {
        return this.year;
    }

    public void setPrice(double price) {
        if (price < 0.00) {
            this.price = 0.00;
        } else {
            this.price = price;
        }
    }

    public double getPrice() {
        return this.price;
    }

    public void setNotebook(boolean isNotebook) {

    }

    public boolean getIsNotebook() {
        return this.isNotebook;
    }

    public void setHardDiskMemory(int hardDiskMemory) {
        if (hardDiskMemory < 0) {
            this.hardDiskMemory = 0;
        } else {
            this.hardDiskMemory = hardDiskMemory;
        }
    }

    public double getHardDiskMemory() {
        return this.hardDiskMemory;
    }

    public void setFreeMemory(double freeMemory) {
        if (freeMemory < 0) {
            this.freeMemory = 0.00;
        } else {
            this.freeMemory = freeMemory;
        }
    }

    public double getFreeMemory() {
        return this.freeMemory;
    }

    public void setOperationSystem(String operationSystem) {
        this.operationSystem = operationSystem;
    }

    public String getOperationSystem() {
        return this.operationSystem;
    }

    public void changeOperationSystem(String newOperationSystem) {
        String[] operationalSystem = {"windows", "linux", "android", "ios"};
        for (int i = 0; i < operationalSystem.length; i++) {
            if (newOperationSystem.equals(operationalSystem[i])) {
                this.operationSystem = newOperationSystem;
            } else {
                this.operationSystem = "undefined";
            }
        }
    }

    public void useMemory(double memory) {
        if (this.freeMemory - memory < 0) {
            System.out.println("Not enough free memory!");
        } else {
            this.freeMemory = this.freeMemory - memory;
        }
    }

    public int comparePrice(Computer other) {
        int var = 5;
        if (this.price < other.price) {
            return -1;
        } else if (this.price == other.price) {
            return 0;
        } else if (this.price > other.price) {
            return 1;
        }
        return var;
    }

    public String toString() {
        String computerToString = "The computer is build in : " + this.year + "\nThe price of the computer is : " + this.price + "\nThe type of the computer is : " + this.isNotebook + "\nThe capacity of the hard drive is : " + this.hardDiskMemory + "\nThe free memory is : " + this.freeMemory + "\nThe operation system of the computer is : " + this.operationSystem;
        return computerToString;
    }
}
