package Nakov_excersize;

import java.util.Scanner;

/**
 * Created by Tonio on 4/25/2017.
 */
public class BogiTaskTriangle {
    public static void main(String[] args) {
        int numberOfTriangles = numberOfTriangles();
        for (int i = 1; i <= numberOfTriangles; i++) {
            System.out.println(getHypotenuseCurrentTriangle(i));
        }
    }

    public static int numberOfTriangles() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("For how much triangles do you want to calculate the Hypotenuses? ");
        int numberOfTriangles = 0;
        do {
            numberOfTriangles = scanner.nextInt();
            if (numberOfTriangles <= 0) {
                System.out.println("The number that you entered is negative or equal to 0. Please try agian!");
            }
        } while (numberOfTriangles <= 0);
        return numberOfTriangles;
    }

    public static double getHypotenuseCurrentTriangle(int i) {
        System.out.println("Enter value of catethus of " + i + " triangle :");
        double cathetusA = CalculateHypotenuses.validateCathetus();
        double cathetusB = CalculateHypotenuses.validateCathetus();
        double hypothenususCurrentTriangle = CalculateHypotenuses.getHypotenuses(cathetusA, cathetusB);
        return hypothenususCurrentTriangle;
    }
}
