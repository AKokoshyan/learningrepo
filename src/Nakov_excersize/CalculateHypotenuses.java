package Nakov_excersize;

import java.util.Scanner;

/**
 * Напишете програма, която по дадени два катета намира хипотенузата на правоъгълен триъгълник.
 * Реализирайте въвеждане на дължините на катетите от стандартния вход, а за пресмятането на хипотенузата използвайте методи на класа Math.
 */
public class CalculateHypotenuses {

    private CalculateHypotenuses() {

    }


    public static double validateCathetus() {
        Scanner scanner = new Scanner(System.in);
        double cathetus = 0.0;
        do {
            System.out.println("Enter a positive number and not equal to 0 ");
            cathetus = scanner.nextDouble();
            if (cathetus <= 0) {
                System.out.println("You have entered negative or equal to 0 number! Please try agin!");
            }
        } while (cathetus <= 0);
        return cathetus;
    }

    public static double getHypotenuses(double cathetusA, double cathetusB) {
        double sumOfCathetus = Math.pow(cathetusA, 2) + Math.pow(cathetusB, 2);
        double calculateHypotenuses = Math.sqrt(sumOfCathetus);
        return calculateHypotenuses;
    }
}