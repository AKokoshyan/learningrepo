/**
 * Created by Tonio on 4/26/2017.
 */
public class Sorting {
    private Sorting() {

    }

    public static void ascendingBubbleSort(int[] array) {
        boolean isSorted = true;
        while (isSorted) {
            isSorted = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] > array[i + 1]) {
                    int temporaryElement = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temporaryElement;
                    isSorted = true;
                }
            }
        }
    }

    public static int[] descendingBubbleSort(int[] array) {
        boolean isSorted = true;
        while (isSorted) {
            isSorted = false;
            for (int i = 0; i < array.length - 1; i++) {
                if (array[i] < array[i + 1]) {
                    int temp = array[i];
                    array[i] = array[i + 1];
                    array[i + 1] = temp;
                    isSorted = true;
                }
            }
        }
        return array;
    }

    public static int[] ascendingSelecionSort(int[] array) {
        int element;
        for (int i = 0; i < array.length - 1; i++) {
            for (int j = i + 1; j < array.length; j++) {
                if (array[i] > array[j]) {
                    element = array[i];
                    array[i] = array[j];
                    array[j] = element;

                }
            }
        }
        return array;
    }
}
