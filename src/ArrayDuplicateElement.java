import java.util.Random;
import java.util.Scanner;

/**
 * Created by Tonio on 4/27/2017.
 */
public class ArrayDuplicateElement {
    private ArrayDuplicateElement() {

    }

    public static int getNumberDuplicateElem(int[] array) {
        int counter = 0;
        int temp = 999999999;
        array = Sorting.ascendingBubbleSort(array);
        for (int i = 0; i < array.length - 1; i++) {
            if (array[i] == array[i + 1] && array[i] != temp) {
                temp = array[i];
                counter++;
            }
        }
        return counter;
    }
}
