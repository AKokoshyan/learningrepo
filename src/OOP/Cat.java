package OOP;

/**
 * Created by Tonio on 6/1/2017.
 */
public class Cat extends Animal {
    private String color;
    private String name;

    //Constructors
    public Cat(){

    }
    //Getters and setters

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}
