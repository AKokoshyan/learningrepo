package OOP;

/**
 * Created by Tonio on 5/31/2017.
 */
public class Car {
    private int maxSpeed;
    private double currentSpeed;
    private String color;
    private int currentGear;
    private Person owner;
    private double price;
    private boolean isSportCar;
    private String model;


    //Constructors
    public Car() {

    }


    //Getters and setters
    public void setCurrentSpeed(double currentSpeed) {
        if (currentSpeed > 0 && currentSpeed < maxSpeed) {
            this.currentSpeed = currentSpeed;
        }
    }

    public double getCurrentSpeed() {
        return this.currentSpeed;
    }

    public void setCurrentGear(int currentGear) {
        if (currentGear > 0 && currentGear < 7) {
            this.currentGear = currentGear;
        }
    }

    public int getCurrentGear() {
        return this.currentGear;
    }

    public void setOwner(Person owner) {
        this.owner = owner;
    }

    public Person getOwner() {
        return this.owner;
    }

    public int getMaxSpeed() {
        return maxSpeed;
    }

    public void setMaxSpeed(int maxSpeed) {
        this.maxSpeed = maxSpeed;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public boolean isSportCar() {
        return isSportCar;
    }

    public void setSportCar(boolean sportCar) {
        isSportCar = sportCar;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }
    //Public methods

    public void accelerate() {
        System.out.println("The car is accelerate !");
    }

    public void changeGearUp() {
        if (currentGear < 6) {
            this.currentGear++;
        }
    }

    public void changeGearDown() {
        if (currentGear > 1) {
            this.currentGear--;
        }
    }

    public void changeGear(int nextGear) {
        if (nextGear > 0 && nextGear < 6) {
            this.currentGear = nextGear;
        }
    }

    public void changeColor(String newColor) {
        this.color = newColor;
    }

    public boolean isMoreExpensive(Car car) {
        boolean isMoreExpensive;
        if (this.price > car.price) {
            isMoreExpensive = true;
        } else {
            isMoreExpensive = false;
        }
        return isMoreExpensive;
    }

    public double calculateCarPriceForScrap(double metalPrice) {
        double price;
        if ((this.color.equals("black") || this.color.equals("white")) && this.isSportCar) {
            price = metalPrice * 0.5;
        } else {
            price = metalPrice * 0.2;
        }
        return price;
    }

    public String toString() {
        String text = "The owner is " + this.owner.getName() + "\nTop speed is " + this.maxSpeed + "\nCurrent speed is " + this.currentSpeed + "\n";
        return text;
    }
}
