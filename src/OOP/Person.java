package OOP;

/**
 * Created by Tonio on 5/31/2017.
 */
public class Person {
    private String name;
    private int age;
    private int personalNumber;
    private char sex;
    private double weight;
    private Person[] friends = new Person[3];
    private double height;
    private boolean isMale;


    //Constructors
    public Person() {
        this.age = 0;
        this.weight = 4.0;
    }

    public Person(String name, int personalNumber, boolean isMale, Person[] friends) {
        this.name = name;
        this.personalNumber = personalNumber;
        this.isMale = isMale;
        this.friends = friends;
    }

    public Person(String name, int age, int personalNumber, char sex, double weight) {
        this.name = name;
        this.age = age;
        this.personalNumber = personalNumber;
        this.sex = sex;
        this.weight = weight;
    }

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public Person(int age, String name, double height) {
        this(age, name);
        this.height = height;
    }
    //Setters and getters

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPersonalNumber() {
        return personalNumber;
    }

    public void setPersonalNumber(int personalNumber) {
        this.personalNumber = personalNumber;
    }

    public char getSex() {
        return sex;
    }

    public void setSex(char sex) {
        this.sex = sex;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public Person[] getFriends() {
        return friends;
    }

    public void setFriends(Person[] friends) {
        this.friends = friends;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public boolean isMale() {
        return isMale;
    }

    public void setMale(boolean male) {
        isMale = male;
    }

    //Public methods

    public void eat() {
        System.out.println(this.name + " is eating !");
    }

    public void walk() {
        System.out.println(this.name + " is walking");
    }

    public void growUp() {
        if (this.age < 105) {
            this.age++;
        }
    }

    public void drinkWater(double water) {
        if (water > 1.0) {
            System.out.println("This is too much water !");
        } else {
            System.out.println(this.name + " is drinking " + water + " water !");
        }
    }

    public String toString() {
        String text = this.getName() + " " + this.getAge();
        return text;
    }
}
