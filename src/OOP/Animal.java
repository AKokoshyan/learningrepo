package OOP;

/**
 * Created by Tonio on 6/1/2017.
 */
public class Animal {
    private int numberEyes;
    private String colorEye;

    //Constructors
    public Animal(){

    }

    //Getters and setters

    public int getNumberEyes() {
        return numberEyes;
    }

    public void setNumberEyes(int numberEyes) {
        this.numberEyes = numberEyes;
    }

    public String getColorEye() {
        return colorEye;
    }

    public void setColorEye(String colorEye) {
        this.colorEye = colorEye;
    }
}
