package HackerRank;

import java.util.Scanner;

/**
 * Created by Tonio on 4/26/2017.
 */
public class JavaLoopsII {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int numberOfIteration = scanner.nextInt();
        for (int i = 0; i < numberOfIteration; i++) {

            int a = scanner.nextInt();
            int b = scanner.nextInt();
            int n = scanner.nextInt();

            int[] array = new int[n];
            int permanent = 0;
            int temporary;
            int sum = permanent;

            for (int j = 0; i < array.length; i++) {
                if (i == 0) {
                    permanent = a + (int) Math.pow(1, i) + b;
                    array[i] = permanent;
                } else {
                    temporary = (int) Math.pow(1, j) + b;
                    sum += temporary;
                    array[i] = sum;
                }
            }
        }
    }

    public static int[] getCustommerNumbers() {
        Scanner scanner = new Scanner(System.in);
        int a = scanner.nextInt();
        int b = scanner.nextInt();
        int n = scanner.nextInt();

        int[] array = new int[n];
        int permanent = 0;
        int temporary;
        int sum = permanent;

        for (int i = 0; i < array.length; i++) {
            if (i == 0) {
                permanent = a + (int) Math.pow(1, i) + b;
                array[i] = permanent;
            } else {
                temporary = (int) Math.pow(1, i) + b;
                sum += temporary;
                array[i] = sum;
            }
        }
        return array;
    }
}
