package HackerRank;

import java.util.Scanner;

/**
 * Created by Tonio on 4/25/2017.
 */
public class JavaIfElse {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int custommerNumber = scanner.nextInt();
        if (custommerNumber >= 1 && custommerNumber <= 100) {
            if (custommerNumber % 2 != 0) {
                System.out.println("Weird");
            } else if ((custommerNumber >= 2 && custommerNumber <= 5) && custommerNumber % 2 == 0) {
                System.out.println("Not Weird");
            } else if ((custommerNumber >= 6 && custommerNumber <= 20) && custommerNumber % 2 == 0) {
                System.out.println("Weird");
            } else if (custommerNumber > 20 && custommerNumber % 2 == 0) {
                System.out.println("Not Weird");
            }
        }
    }
}
