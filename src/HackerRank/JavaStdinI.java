package HackerRank;

import java.util.Scanner;

/**
 * Created by Tonio on 4/25/2017.
 */
public class JavaStdinI {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int custommerNumber = scanner.nextInt();
        double custommerDouble = scanner.nextDouble();
        scanner.nextLine();
        String custommerString = scanner.nextLine();
        
        System.out.println("String: " + custommerString);
        System.out.println("Double: " + custommerDouble);
        System.out.println("Int: " + custommerNumber);
    }
}
