package HackerRank;

import java.util.Scanner;

/**
 * Created by Tonio on 4/27/2017.
 */
public class Datatypes {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int custommerNumber = scanner.nextInt();
        int[] array = new int[custommerNumber];
        for (int i = 0; i < array.length; i++) {
            array[i] = scanner.nextInt();
        }
        long s = 1500000000;
        for (int i = 0; i < array.length; i++) {

            if (array[i] >= Math.pow(2, 63) - 1 && array[i] <= Math.pow(2, 63 - 1)) {
                System.out.println(array[i] + "can be fitted in:");
                System.out.println("* long");
                System.out.println("* int");
                System.out.println("* short");
                System.out.println("* byte");
            }
            if (array[i] >= Math.pow(2, 31) - 1 && array[i] <= Math.pow(2, 31) - 1) {
                System.out.println(array[i] + "can be fitted in:");
                System.out.println("* int");
                System.out.println("* short");
                System.out.println("* byte");
            }
            if (array[i] >= Math.pow(2, 15) - 1 && array[i] <= Math.pow(2, 15) - 1) {
                System.out.println(array[i] + "can be fitted in:");
                System.out.println("* short");
                System.out.println("* byte");
            }
            if (array[i] >= -128 && array[i] <= 127) {
                System.out.println(array[i] + "can be fitted in:");
                System.out.println("* byte");
            }
        }
    }
}

