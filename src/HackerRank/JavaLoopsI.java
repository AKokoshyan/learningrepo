package HackerRank;

import java.util.Scanner;

/**
 * Created by Tonio on 4/25/2017.
 */
public class JavaLoopsI {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int custommerNumber = scanner.nextInt();
        multiplyNumbersToTen(custommerNumber);
    }

    public static void multiplyNumbersToTen(int custommerNumber) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(custommerNumber + " x " + i + " = " + custommerNumber * i);
        }
    }
}
