/**
 * Created by Tonio on 4/23/2017.
 */
public  class Person {
    private String name;
    private int age;
    private String personalNumber;
    private String sex;
    private double weight;
    private Person[] friends;
    private double height;
    private boolean isMale;
    private double money;
    private Car ownCar;
    private Person friend;

    public Person(){

    }
    public Person(String name, String personalNumber, boolean isMale) {
        this.age = 0;
        this.weight = 4.0;
        this.name = name;
        this.personalNumber = personalNumber;
        this.isMale = isMale;
    }

    public Person(String name, String personalNumber, boolean isMale, Person[] newArray) {
        this(name, personalNumber, isMale);
        this.friends = newArray;
    }

    public Person(int age, String name) {
        this.age = age;
        this.name = name;
    }

    public Person(int age, String name, double height) {
        this.age = age;
        this.name = name;
        this.height = height;

    }

    public Person(String name, int age, String personalNumber, String sex, double weight) {
        this.name = name;
        this.age = age;
        this.personalNumber = personalNumber;
        this.sex = sex;
        this.weight = weight;
    }

    public String getName() {
        return this.name;
    }

    public void eat() {
        System.out.println(this.name + " is eating!");
    }

    public void walk() {
        System.out.println(this.name + " is walking!");
    }

    public void growUp() {
        if (age >= 0 && age <= 100) {
            age++;
        }
    }

    public void drinkWater(double liters) {
        if (liters > 1.0) {
            System.out.println("This is too much water");
        } else {
            System.out.println(this.name + " is drinking " + liters + " water!");
        }
    }

    public void buyCar(Car car){
        if(this.money >=car.getPrice()){
           this.ownCar = car;
           car.setOwner(this);
        }
    }
}
