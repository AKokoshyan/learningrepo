/**
 * Created by Tonio on 4/19/2017.
 */
public class Student {
    private String name;
    private String subject;
    private double grade;
    private int yearInCollege;
    private int age;
    private boolean isDegree;
    private double money;

    public Student() {
        this.grade = 4.0;
        this.yearInCollege = 1;
        this.isDegree = false;
        this.money = 0;
    }

    public Student(String name, String subject, int age) {
        this();
        this.name = name;
        this.subject = subject;
        this.age = age;
    }

    public void setGrade(double grade) {
        if (grade >= 2.0 && grade <= 6.0) {
            this.grade = grade;
        }
    }
    public String getName(){
        return this.name;
    }

    public void setSubject(String subject){
        this.subject = subject;
    }
    public String getSubject(){
        return this.subject;
    }

    public double getGrade() {
        return this.grade;
    }

    public void setYearInCollege(int yearInCollege) {
        if (yearInCollege >= 1 && yearInCollege <= 4) {
            this.yearInCollege = yearInCollege;
        }
    }

    public int getYearInCollege() {
        return this.yearInCollege;
    }

    public void upYear() {
        if (isDegree == false && yearInCollege < 4) {
            ++this.yearInCollege;
            if (yearInCollege == 4) {
                this.isDegree = true;
            }
        }
    }

    public double receiveStudentship(double min, double amount) {
        if (grade >= min && age < 30) {
            return this.money = this.money + amount;
        } else {
            return this.money;
        }
    }

    public String toString() {
        String studentToString = "The name of the student is : " + this.name + "\nThe student is studieng " + this.subject + "\nThe grade of the student is : " + this.grade + "\nThe student is " + this.yearInCollege + " year in college" + "\nThe age of the student is " + this.age + "\nIs the student still studieng ? : " + this.isDegree + "\nThe student is earned " + this.money + " from studentship!";
        return studentToString;
    }
}

