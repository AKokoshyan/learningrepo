import java.util.Scanner;

public class Task2LocalShop {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String custemerProduct = scanner.nextLine().toLowerCase();
		String custemerTown = scanner.nextLine().toLowerCase();
		Double quantity = Double.parseDouble(scanner.nextLine());

		if (custemerTown.equals("sofia")) {
			if (custemerProduct.equals("coffee")) {
				System.out.println(quantity * 0.5);
			} else if (custemerProduct.equals("water")) {
				System.out.println(quantity * 0.8);
			} else if (custemerProduct.equals("beer")) {
				System.out.println(quantity * 1.2);
			} else if (custemerProduct.equals("sweets")) {
				System.out.println(quantity * 1.45);
			} else if (custemerProduct.equals("peanuts")) {

				System.out.println(quantity * 1.6);
			}
		} else if (custemerTown.equals("plovdiv")) {
			if (custemerProduct.equals("coffee")) {
				System.out.println(quantity * 0.4);
			} else if (custemerProduct.equals("water")) {
				System.out.println(quantity * 0.7);
			} else if (custemerProduct.equals("beer")) {
				System.out.println(quantity * 1.15);
			} else if (custemerProduct.equals("sweets")) {
				System.out.println(quantity * 1.30);
			} else if (custemerProduct.equals("peanuts"))
				System.out.println(quantity * 1.5);
		} else if (custemerTown.equals("varna")) {

			if (custemerProduct.equals("coffee")) {
				System.out.println(quantity * 0.45);
			} else if (custemerProduct.equals("water")) {
				System.out.println(quantity * 0.7);
			} else if (custemerProduct.equals("beer")) {
				System.out.println(quantity * 1.1);
			} else if (custemerProduct.equals("sweets")) {
				System.out.println(quantity * 1.35);
			} else if (custemerProduct.equals("peanuts")) {

				System.out.println(quantity * 1.55);
			}
		}
	}
}