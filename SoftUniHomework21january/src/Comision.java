import java.util.Scanner;

public class Comision {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String town = scanner.nextLine().toLowerCase();
		double s�lls = Double.parseDouble(scanner.nextLine());

		if (town.equals("sofia")) {
			if (s�lls >= 0 && s�lls <= 500) {
				System.out.println(s�lls * 5 / 100);
			} else if (s�lls > 500 && s�lls <= 1000) {
				System.out.println(s�lls * (7 / 100));
			} else if (s�lls > 1000 && s�lls <= 10000) {
				System.out.println((s�lls * (8 / 100)));
			} else if (s�lls > 10000) {
				System.out.println(s�lls * (12 / 100));
			}
		} else if (town.equals("varna")) {
			if (s�lls >= 0 && s�lls <= 500) {
				System.out.println(s�lls * (4.5 / 100));
			} else if (s�lls > 500 && s�lls <= 1000) {
				System.out.println(s�lls * (7.5 / 100));
			} else if (s�lls > 1000 && s�lls <= 10000) {
				System.out.println(s�lls * (10 / 100));
			} else if (s�lls > 10000) {
				System.out.println(s�lls * (13 / 100));
			}
		} else if (town.equals("plovdiv")) {
			if (s�lls >= 0 && s�lls <= 500) {
				System.out.println(s�lls * (5.5 / 100));
			} else if (s�lls > 500 && s�lls <= 1000) {
				System.out.println(s�lls * (8 / 100));
			} else if (s�lls > 1000 && s�lls <= 10000) {
				System.out.println(s�lls * (12 / 100));
			} else if (s�lls > 10000) {
				System.out.println(s�lls * (14.5 / 100));
			}
		} else {
			System.out.println("error");
		}

	}

}
