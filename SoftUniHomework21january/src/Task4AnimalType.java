import java.util.Scanner;

public class Task4AnimalType {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String animalType = scanner.nextLine().toLowerCase();

		switch (animalType) {
		case "dog":
			System.out.println("mammal");
			break;
		case "crocodile":
		case "tortoise":
		case "snake":
			System.out.println("reptile");
			break;
		default:
			System.out.println("unknown");

		}

	}

}
