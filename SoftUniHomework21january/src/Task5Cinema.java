import java.util.Scanner;

public class Task5Cinema {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String type = scanner.nextLine().toLowerCase();
		int row = scanner.nextInt();
		int colon = scanner.nextInt();
		int sum = row * colon;
		double a = 12.00;
		double b = 7.50;
		double c = 5.00;

		if (type.equals("premiere")) {
			System.out.println(sum * a + " leva");
		} else if (type.equals("normal")) {
			System.out.println(sum * b + " leva");
		} else {
			System.out.println(sum * c + " leva");
		}
	}

}
